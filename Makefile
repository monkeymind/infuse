# Makefile - for your comfort and convenience.
# This file is part of INFUSE.
# Copyright (C) 1997, 1998 by Florian M. Weps.  All rights reserved.

# for debugging and profiling (with gprof):
# CFLAGS = -g -pg -Wformat -I/usr/include/ncurses
# CFLAGS = -g -Wall -ansi -pedantic -I/usr/include/ncurses
# for an optimized compilation:
# CFLAGS = -O3 -I/usr/include/ncurses

#CFLAGS = -g -Wchar-subscripts -Wformat -Wimplicit -Wreturn-type -Wswitch -Wunused -Wshadow -Wmissing-declarations -Isys/linux-x11 -I/usr/include/ncursesw
CFLAGS = -g -Isys/linux-x11 -I/usr/include/ncursesw -Iblorblib -DINFUSE_REVISION=\"`git describe --always --tags`\" -DTRACE=0
#CFLAGS = -g -Isys/linux-x11 -I/usr/include/ncursesw
CC = gcc

FWFLAGS = +d +q
FW = fw

XLIBPATH = -L/usr/X11R6/lib
XLIB = -lXaw -lXmu -lXt -lSM -lICE -lXext -lX11
NCURSES = -lncursesw

all: xinfuse ninfuse infuse

xinfuse : core.o ztex11.o Zork.o blorblib.o
	$(CC) $(CFLAGS) -o xinfuse core.o blorblib.o ztex11.o Zork.o $(XLIBPATH) $(XLIB)

ninfuse : core.o ztncur.o blorblib.o
	$(CC) $(CFLAGS) -o ninfuse core.o blorblib.o ztncur.o $(NCURSES)

infuse : core.o ztstdio.o blorblib.o
	$(CC) $(CFLAGS) -o infuse core.o blorblib.o ztstdio.o

core.c : core.fw
	$(FW) $(FWFLAGS) core.fw

core.o : core.c zterm.h blorblib/blorb.h
	$(CC) $(CFLAGS) -c core.c -o core.o

ztex11.o : sys/linux-x11/ztex11.c sys/linux-x11/Zork.h zterm.h 
	$(CC) $(CFLAGS) -I./sys/linux-x11 -I. -c sys/linux-x11/ztex11.c -o ztex11.o

Zork.o : sys/linux-x11/Zork.c sys/linux-x11/ZorkP.h sys/linux-x11/Zork.h zterm.h
	$(CC) $(CFLAGS) -I./sys/linux-x11 -I. -c sys/linux-x11/Zork.c -o Zork.o

ztncur.o : sys/linux-ncurses/ztncur.c zterm.h
	$(CC) $(CFLAGS) -I. -I/usr/include/ncurses -c sys/linux-ncurses/ztncur.c -o ztncur.o

ztstdio.o : sys/linux-stdio/ztstdio.c zterm.h
	$(CC) $(CFLAGS) -I. -c sys/linux-stdio/ztstdio.c -o ztstdio.o

blorblib.o : blorblib/blorblib.c blorblib/blorb.h blorblib/blorblow.h
	$(CC) $(CFLAGS) -c blorblib/blorblib.c -o blorblib.o

clean :
	rm -f core.c *.o *.lis xinfuse ninfuse infuse
