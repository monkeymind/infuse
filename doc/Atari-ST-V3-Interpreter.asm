;;; ZIP5A - Atari ST interpreter version A
	TEXT
	XDEF zmachin	; the entry point, called from C program
	XREF debugbc	; debug C function, takes a word parameter
	XREF a6_sav	; GEMDOS clobbers a6 but we need it
	XREF ncolumns	; number of columns on screen
	XREF text_top	; upper edge of lower window
	XREF clearlin
	XREF crsr_lin
	XREF crsr_col
	XREF clear_sc
	XREF new_time
	XREF print_ti
	XREF get_a_ch
	XREF print_a_
	XREF nlines
	XREF print
	XREF open_sto
	XREF closef
	XREF read_ite
	XREF open_fna
	XREF asksaven
	XREF create_f
	XREF write_it
	XREF close_fi
	XREF delete_f
	XREF open_fil
	XREF file_pat
	XREF milli_se
	XREF milli_co
	XREF old_time
w14		EQU	-2	; w
s_t_flag	EQU	-4	; w - score or time game
permblocks	EQU	-6	; w - # blocks below high memory mark
w15		EQU	-8	; w
l17		EQU	-12	; l
obj_tab		EQU	-16	; l
globals		EQU	-20	; l
l11		EQU	-24	; l
kbincod		EQU	-28	; l - list of keyboard input codes (.,")
kbinclim	EQU	-32	; l - byte beyond last keyboard input code.
dentlen		EQU	-34	; w - dictionary entry length
ndentry		EQU	-36	; w - number of dictionary entries
dictdat		EQU	-40	; l - pointer to dictionary entries
lastdic		EQU	-44	; l - end of dictionary words
fltblktab	EQU	-48	; l -
fltblockp	EQU	-52	; l -
fltblocks	EQU	-54	; w
a7save		EQU	-60	; l
stackbot	EQU	-64	; l
block_number	EQU	-66	; w - virtual memory page number of PC
block_index	EQU	-68	; w - virtual memory page index of PC
currframe	EQU	-72	; l - frame pointer
randomseed	EQU	-74	; w - a random number generator seed
random2seed	EQU	-76	; w - another seed
;;; 4 unused bytes
blockp		EQU	-84	; l -
;;; 8 unused bytes
param3		EQU	-96	; l -
param1		EQU	-100	; l -
nparams		EQU	-102	; w -
wordbuf		EQU	-112	; 10 bytes (6 in V3 - extension for V4++)
dicwordbuf	EQU	-118	; 6 bytes = 9 z-chars (6 in V3, 9 in V4++)
sread_text	EQU	-122	; l - points to input buffer in story
eoinput		EQU	-126	; l - end of typed input in above buffer
sread_parse	EQU	-130	; l - points to parse buffer in story
nwords		EQU	-132	; b - number of words parsed
bofword		EQU	-136	; l - points to beginning of word lexed
w9		EQU	-138	; w
w1		EQU	-140	; w
;;; 2 unused bytes
mystrflag	EQU	-144	; w
;;; 8 unused bytes
conbufflag	EQU	-154	; w
;;; 2 unused bytes
constring	EQU	-160	; l
lpstring	EQU	-164	; l
;;; 8 unused bytes
currblock_p	EQU	-176	; l
currblock	EQU	-178	; w
l23		EQU	-182	; l
fltblkid	EQU	-186	; l
currfltblk	EQU	-188	; w
currfltblk_p	EQU	-192	; l
currfltrec	EQU	-196	; l
;;; 10 unused bytes
sel_wind	EQU	-208	; w - 0 = lower, 1 = upper wind
old_col		EQU	-210	; w - save area when switching
old_line	EQU	-212	; w - between upper & lower
lines_printed	EQU	-214	; w
pending_input	EQU	-216	; w
inbufcnt	EQU	-218	; w
;;; 6 unused bytes
mystring	EQU	-228	; l
l6		EQU	-232	; l
l7		EQU	-236	; l
;;; 8 unused bytes
story_handle	EQU	-246	; w
save_handle	EQU	-248	; w
	
zmachin link	a6,#-262	; claim 262 bytes for local variables
	movea.l a6,a0
	move.w	#131,D0		; 131 words = 262 bytes
1$	clr.w	-(a0)
	subq.w	#1,D0
	bne.s	1$
	move.l	a7,a7save(a6)	; save a7
	lea.l	a6_sav,a0
	move.l	a6,(a0)		; save a6 in a6_sav
	move.w	#1,w1(a6)	; w1(a6) = 1
	bsr	initstoryfile	; open the story file
	sub	#512,a7		; 512 byte buffer for one file block
	movea.l a7,a4		; address of buffer in a4
	movea.l a4,a0		; buffer address for readblock
	clr.w	d0		; block number 0
	bsr	readblock	; read block 0 into buffer starting at a0.
	bsr	qfreemem	; how much memory is left? (-10k for fsel)
	move.l	d0,d7		; d7  free memory.
	moveq.l #0,d0		; clear d0
	move.w	26(a4),d0	; header word $1A (length of file)
	add.l	d0,d0		; d0 *= 2
	cmp.l	d7,d0		; do we have enough memory?
	blt.s	2$		; yes, go to 2$
	moveq.l #0,d0		; maybe not, so clear d0
	move.w	4(a4),d0	; mem test the second
	cmp.l	d7,d0		; enough?
	blt.s	3$		; yes
	bra	mem_error	; really not, trap error
2$	bsr	udiv512rnd	; divide memory requirement by 512
	move.w	d0,permblocks(a6) ; permblocks(a6) memory requirement
				; in blocks
	move.w	#2,fltblocks(a6) ; number of free blocks  2 (at least)
	bra.s	5$		; skip treatment for the second memory test
3$	sub.l	d0,d7		; d7 -= d0  adjust free memory
	bsr	udiv512rnd	; divide memory requirement by 512
	move.w	d0,permblocks(a6) ; permblocks(a6)
				; memory requirement in blocks
	divu.w	#520,d7		; divide free memory by 520
	cmpi.w	#2,d7		; room for two or more blocks?
	bge.s	4$		; yes
	moveq.l #2,d7		; no, let's say 2
4$	move.w	d7,fltblocks(a6) ; anyway, fltblocks(a6)  #free memory blocks
5$	move.w	permblocks(a6),d0 ; now, let's allocate memory
	bsr	utimes512	; block number times block size
	bsr	Malloc		; ask GEMDOS
	move.l	a0,blockp(a6)	; blockp(a6)  address of allocated storage.
	movea.l a0,a4		; address of buffer (blockp(a6))
	move.w	permblocks(a6),d1 ; number of blocks to load
	clr.w	d0		; from beginning of file
	bsr	readitem	; read them
	cmpi.b	#3,(a4)		; first byte  Z-machine version
	beq.s	7$		; Do we have a V3 application?
6$	clr.w	d0		; No! Scream and trap error.
	lea.l	wrongz,a0	; "Wrong Z-machine version"
	bra	internal_error	; there; it's all your fault anyway.
7$	move.w	$2(a4),relno(a6) ; release number
	btst.b	#0,1(a4)	; is bit zero of byte 1 set?
	bne.s	6$		; yes! scream etc.
	clr.w	s_t_flag(a6)	; s_t_flag(a6) = 0
	btst.b	#1,1(a4)	; is this a ``time'' game?
	beq.s	8$		; no, skip this
	addq.w	#1,s_t_flag(a6) ; well, s_t_flag(a6) is 1 then.
8$	moveq.l #0,d0		; anyway, clear d0
	move.w	8(a4),d0	; location of dictionary
	add.l	a4,d0		; plus base of loaded blocks
	move.l	d0,dictptr(a6)	; 68k pointer to dictionary
	moveq.l #0,d0		; clear d0
	move.w	10(a4),d0	; another offset in word 5
	add.l	a4,d0		; compute address with this offset
	move.l	d0,obj_tab(a6)	; and store it in obj_tab(a6)
	moveq.l #0,d0		; clear d0
	move.w	12(a4),d0	; start of global variables in word 6
	add.l	a4,d0		; yes, compute new address
	move.l	d0,globals(a6)	; and store it in globals(a6)
	moveq.l #0,d0		; clear d0
	move.w	24(a4),d0	; offset in word 12
	add.l	a4,d0		; compute new address
	move.l	d0,abbrevp(a6)	; and store it in abbrevp(a6)
	moveq.l #0,d0		; clear d0
	move.w	14(a4),d0	; word 7
	bsr	udiv512rnd	; divide by 512
	move.w	d0,statmem(a6)	; and store it in statmem(a6)
	move.l	#512,d0		; get another block
	bsr	Malloc		; beg GEMDOS
	move.l	a0,stackbot(a6) ; and put address into stackbot(a6)
	move.w	#256,d1		; buffer size
	move.w	ncolumns,d2	; number of columns
	lea.l	cprint,a1	; output fun for screen
	lea.l	otherfun,a2	; otherfun for screen
	bsr	makeopstruct	; build console output string struct
	move.l	a0,constring(a6) ; store address of struct
	move.w	#80,d1		; buffer size
	move.w	#80,d2		; printer has eighty columns
	lea.l	lprint,a1	; printer output fun
	lea.l	otherfun,a2	; otherfun for printer
	bsr	makeopstruct	; build line printer output string struct
	move.l	a0,lpstring(a6) ; store address of struct
	moveq.l #80,d0		; we also want 80 bytes for another buffer...
	bsr	Malloc		; ...and beg GEMDOS for it...
	move.l	a0,kbincod(a6)	; ...so we can store it in kbincod(a6).
	movea.l dictptr(a6),a1	; now, we copy the dictionary header
	clr.w	d0		; into the new buffer.
	move.b	(a1)+,d0	; number of input codes
9$	move.b	(a1)+,(a0)+	; copy it
	subq.w	#1,d0		; keep track
	bne.s	9$		; next one
	move.l	a0,kbinclim(a6) ; kbinclim(a6)	end of list of kb input codes
	lea.l	strange,a2	; `strange' is zero-terminated list:
				; SPC, TAB, CR, `.', `,', `?'
10$	move.b	(a2)+,(a0)+	; copy it
	bne.s	10$		; while not 0
	clr.w	d0		; clear d0
	move.b	(a1)+,d0	; entry length
	move.w	d0,dentlen(a6)	; into dentlen(a6)
	move.b	(a1)+,d0	; high order byte
	asl.w	#8,d0		; times 256
	move.b	(a1)+,d0	; low order byte
	move.w	d0,ndentry(a6)	; number of dictionary entries
	move.l	a1,dictdat(a6)	; dictdat(a6) pointer to entries
	subq.w	#1,d0		; d0--
	mulu.w	dentlen(a6),d0	; compute an offset...
	adda.l	d0,a1		; ...and with it, the address...
	move.l	a1,lastdic(a6)	; ...of the last dictionary word
	move.w	fltblocks(a6),d0 ; fltblocks(a6)
	mulu.w	#8,d0		; times 8
	addq.l	#2,d0		; plus 2
	bsr	Malloc		; beg GEMDOS
	move.l	a0,fltblktab(a6) ; fltblktab(a6)  table of
				; contents for floating blocks
	move.w	fltblocks(a6),d0 ; fltblocks(a6)
	mulu.w	#512,d0		; times 512
	bsr	Malloc		; beg GEMDOS
	move.l	a0,fltblockp(a6) ; fltblockp(a6) is base of floating blocks
	move.w	fltblocks(a6),d0 ; fltblocks(a6)
	movea.l fltblktab(a6),a0 ; and the other buffer (table of contents?)
11$	move.w	#-2,(a0)+	; init the table
	clr.l	(a0)+
	clr.w	(a0)+
	subq.w	#1,d0
	bne.s	11$		; next entry
	move.w	#-1,(a0)	; last entry  -1
	move.w	#-1,currblock(a6) ; currblock(a6)  -1
	move.w	#-1,currfltblk(a6) ; currfltblk(a6)  -1
;;; Init the Random Number Generator
	bsr	Random		; hello XBIOS
	move.w	d0,randomseed(a6) ; randomseed(a6)  random number
	swap.w	d0		; high word
	move.w	d0,random2seed(a6) ; random2seed(a6)  second random number
;;; init conbufflag
	move.w	#1,conbufflag(a6) ; conbufflag(a6) = 1
	bra.s	astart		; skip the `restart' code
rstart	movea.l blockp(a6),a0
	move.w	16(a0),-(a7)	; save the `Flags 2' field (transcript bit)
	clr.w	d0
	move.w	statmem(a6),d1
	bsr	readitem
	movea.l blockp(a6),a0
	move.w	(a7)+,16(a0)
astart	movea.l a7save(a6),a7	; restore a7
	movea.l constring(a6),a0 ; a0  console output struct
	move.l	0(a0),6(a0)	; clear buffer
	movea.l stackbot(a6),a4 ; load bytecode stack pointer
	add	#512,a4		; end of buffer
	move.l	a4,currframe(a6) ; currframe is end of buffer stackbot
	subq.l	#2,currframe(a6) ; currframe is last word in stackbot
	movea.l blockp(a6),a2	; big block buffer
	bset.b	#5,1(a2)	; screen splitting available
	move.w	#1,text_top	; make room for title bar
	move.w	6(a2),d0	; get start address
	bsr	badr2adr	; divide by 512
	move.w	d0,block_number(a6) ; the quotient
	move.w	d1,block_index(a6) ; the remainder
	bsr	swapblock	; make sure block block_number(a6) is in memory
	bsr	donuttin	; does nothing, just returns to here.
	bra	bytecode_machine ; start the game

*--- utimes512 ---*
* parameters  d0  a word
* returns  a longword
* side effects	unsigned word is extended to long and multiplied with 2.

utimes512
	ext.l	d0
	swap.w	d0
	lsr.l	#7,d0
	rts

*--- udiv512rnd ---*
* parameters  d0  a word
* returns  a word
* side effects	divides d0 by 512 and rounds up.

udiv512rnd
	move.w	d0,-(a7)
	lsr.l	#8,d0
	lsr.l	#1,d0		; /512
	andi.w	#511,(a7)+
	beq.s	1$
	addq.w	#1,d0
1$	rts

*--- getint ---*
* parameters: a0  char *
* returns: int
* side effects: reads two subsequent bytes from a0 and makes an int out of them

getint
	move.b	(a0)+,d0
	asl.w	#8,d0
	move.b	(a0)+,d0
	rts

*--- moveword ---*
* parameters: a0: address to put the word
*	      d0: the word
* returns: nothing
* side effects: puts the word at the address (which doesn't have to be even)

moveword
	move.b	d0,1(a0)
	asr.w	#8,d0
	move.b	d0,(a0)
	rts

*--- fetchbyte ---*
* parameters: d0: block number
*	      d1: block index
* returns: d2: the desired byte
* side effects: may swap blocks

fetchbyte
	move.w	d0,-(a7)		; save block number
	cmp.w	permblocks(a6),d0	; is it a permanent block
	bge.s	3$			; no!
	bsr.s	utimes512		; yes, block number*512
	or.w	d1,d0			; or them (add, but quick?)
	clr.w	d2			; clear this
	movea.l blockp(a6),a0		; get block base
	move.b	0(a0,d0.l),d2		; get this byte
	bra.s	1$			; skip the block swapping
3$	bsr	swapfltblk		; swap in block
	clr.w	d2			; clear word to output
	move.b	0(a0,d1.w),d2		; get byte
1$	move.w	(a7)+,d0		; restore old block number
	addq.w	#1,d1			; next byte
	cmpi.w	#512,d1			; are we at end of block
	bne.s	2$			; no
	clr.w	d1			; yes, adjust d1
	addq.w	#1,d0			; and d0
2$	rts				; and return

*--- fetchint ---*
* parameters: d0: block number
*	      d1: block index
* returns: d2: the desired int
* side effects: may swap blocks

fetchint
	bsr.s	fetchbyte
	asl.w	#8,d2
	move.w	d2,-(a7)
	bsr.s	fetchbyte
	or.w	(a7)+,d2
	rts

*--- fetch_bytecode ---*
* parameters:  none
* returns: d0: byte at current address
* side effects: updates block_index

fetch_bytecode
	movea.l currblock_p(a6),a0	; base of current block
	add	block_index(a6),a0	; current byte in block
	clr.w	d0			; clear word for bytecode
	move.b	(a0),d0			; fetch bytecode
	addq.w	#1,block_index(a6)	; next byte in block
	cmpi.w	#512,block_index(a6)	; are we at the end?
	blt.s	1$			; no, don't worry.
	move.w	d0,-(a7)		; yes, save bytecode for later
	bsr	swapblock		; swap in next block
	move.w	(a7)+,d0		; restore bytecode
1$	rts				; return.

*--- fetch16bit ---*
* parameters: none
* returns: d0: the (word) parameter
* side effects

fetch16bit
	bsr.s	fetch_bytecode
	asl.w	#8,d0
	move.w	d0,-(a7)
	bsr.s	fetch_bytecode
	or.w	(a7)+,d0
	rts

*--- fetch_parameters ---*
* parameter: d0 parameter modifier.
*	     0: fetch a 16-bit parameter (word)
*	     1: fetch an 8-bit parameter (byte)
*	     2: fetch a 16-bit parameter (word) from some stackbot buffer
* returns: the desired parameter
* side effects:

fetch_parameters
	subq.w	#1,d0
	blt.s	fetch16bit	; fetch a word-sized parameter
	beq.s	fetch_bytecode	; fetch a byte-sized parameter
	bsr.s	fetch_bytecode	; fetch indirection
	tst.w	d0		; is it zero (means ``pop from stack'')?
	bne.s	recall_int	; no, get from memory
	move.w	(a4)+,d0	; yes; pop from bytecode stack
	rts

*--- bytecode rclint and recall_int ---*
* parameters for rclint: d0: 0	  : return top of stack
*			     other: call recall_int with it.
*	     for recall_int: d0:
* returns: int
* side effects

rclint
	tst.w	d0
	bne.s	recall_int	; get word from memory
	move.w	(a4),d0		; make copy of stack
	rts
recall_int
	cmpi.w	#16,d0		; parameter < 16?
	bge.s	2$		; no
	movea.l currframe(a6),a0	; current stack frame
	subq.w	#1,d0		; index - 1
	add.w	d0,d0		; times 2 (word table)
	sub	d0,a0		; move down in frame
	move.w	(a0),d0		; get that word
	rts
2$	sub.w	#16,d0		; parameter -16
	movea.l globals(a6),a0	; globals
	add.w	d0,d0		; times 2 for word table
	add	d0,a0		; word to read
	bra	getint		; get 2 bytes from a0 and make a word

;;; storeint - store a value into a variable
;;; parameters:	d0 - variable number
;;;                  0        - write into top of stack (DON'T PUSH)
;;;                  1 ... 15 - intlo local variable
;;;                  16 . 255 - into global variable

storeint
	tst.w	d0		; test parameter
	bne.s	1$		; is it 0?
	move.w	d1,(a4)		; yes. put d1 on stack and return
	rts
1$	cmpi.w	#16,d0		; d0 < 16?
	bge.s	2$		; no, go to 2$
	movea.l currframe(a6),a0	; get currframe
	subq.w	#1,d0		; d0--
	add.w	d0,d0		; d0*=2
	sub	d0,a0		; down into buffer
	move.w	d1,(a0)		; put d1 there
	rts
2$	sub.w	#16,d0		; shift d0 down
	movea.l globals(a6),a0	; get globals
	add.w	d0,d0		; d0*=2
	add	d0,a0		; up into buffer
	move.w	d1,d0		; param for moveword
	bra	moveword		; call moveword

*--- pushbyte and pushword ---*
* parameters: d0 thing to push
* returns: nothing
* side effects: pushes thing into indirection at curent bytecode

pushbyte
	andi.w	#$FF,d0		; mask out high byte
pushword
	move.w	d0,d1		; save it
	bsr	fetch_bytecode	; get next bytecode
	tst.w	d0		; is it empty?
	bne.s	storeint	; no
	move.w	d1,-(a4)	; yes. put d1 on bytecode stack.
	rts			; return.

*--- jumpf and jumpt ---*
* parameters: none
* returns: nothing
* side effects: examines the next address and
*		jumpf: jumps there if the skip bit is cleared
*		jumpt: jumps there if the skip bit is set

jumpf
	clr.w	d1		; jumpf starts with 0 in d1
	bra.s	LLL_2
jumpt
	moveq.l #1,d1		; jumpt starts with 1 in d1
LLL_2	bsr	fetch_bytecode	; get next bytecode
	bclr.l	#7,d0		; clear bit 7
	beq.s	1$		; was it set?
	addq.w	#1,d1		; yes, add 1 to d1 (make it true)
1$	bclr.l	#6,d0		; clear bit 6
	bne.s	2$		; was it set?
	asl.w	#8,d0		; no. make room for second bytecode
	move.w	d0,d2		; and save it in d2
	bsr	fetch_bytecode	; get next bytecode
	or.w	d2,d0		; or the two together
	btst.l	#13,d0		; test bit 13
	beq.s	2$		; was it set?
	ori.w	#$C000,d0	; yes. set the two high bits
2$	subq.w	#1,d1		; is d1 == 1?
	beq.s	5$		; yes? skip the jump
	tst.w	d0		; test d0
	bne.s	3$		; empty?
	bra	RFALSEop	; yes. go to RFALSEop (calls RETop with 0)
3$	subq.w	#1,d0		; is d0 == 1?
	bne.s	4$		; no?
	bra	RTRUEop		; yes. go to RTRUEop (calls RETop with 1)
4$	subq.w	#1,d0		; decrease d0, add it to block_index of pc
	add.w	d0,block_index(a6)
	bra	swapblock	; refresh memory
5$	rts			; return.

;;; badr2adr - compute the segemtn number and index from a byte address
;;; parameters:	d0 - byte address
;;; returns:	d0 - segment number
;;;             d1 - segment index
badr2adr
	move.w	d0,d1
	lsr.w	#8,d0
	lsr.w	#1,d0		; divide by 512
	andi.w	#511,d1		; remainder
	rts

;;; pard2adr - unpack a packed address into segment number and index.
;;; parameters:	d0 - packed address
;;; returns:	d0 - segment number
;;;             d1 - segment index
padr2adr
	move.w	d0,d1		; save d0
	lsr.w	#8,d0		; d0 /= 256
	andi.w	#255,d1		; mask out high bits
	add.w	d1,d1		; multiply by 2
	rts
;;; unused but present - V4 padr2adr
;;; This is padr2adr for a pack factor of 4, as would be used in V4.
	move.w	d0,d1		; copy d0
	lsr.w	#7,d0		; d0 /= 128
	andi.w	#127,d1		; mask out high bits
	add.w	d1,d1		; multiply by 4
	add.w	d1,d1		; (second part of multiply by 4)
	rts

*--- bytecode_base bytecode base ---*
* this address was used by the IMPLEMENTORS to compute the bytecode
* routine addresses from the function offset tables

bytecode_base
	nop

*--- bytecode ADDop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: adds the two parameters and stores them in indirection

ADDop
	add.w	d1,d0		; p1 + p2
	bra	pushword	; ->(ind)

*--- bytecode SUBop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: p1-p2->(ind)

SUBop
	sub.w	d1,d0		; p1 - p2
	bra	pushword	; ->(ind)

*--- bytecode MULop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: p1*p2->(ind)

MULop
	muls.w	d1,d0		; p1 * p2
	bra	pushword	; ->(ind)

*--- bytecode DIVop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: p1/p2->(ind)

DIVop
	ext.l	d0		; make it long
	divs.w	d1,d0		; divide
	bra	pushword	; push

*--- bytecode MODop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: p1%p2->(ind)

MODop
	ext.l	d0		; make it long
	divs.w	d1,d0		; divide
	swap.w	d0		; get remainder
	bra	pushword	; push

;;; RANDOMop - generate a random number (operand > 0) / re-seed the generator 
;;; (operand < 0) or randomize the generator (operand = 0). At least, that's
;;; what the standard says. This here just returns a random number.

RANDOMop
	tst.w	d7			; test d7
	move.w	random2seed(a6),d1	; load first random seed
	swap.w	d1			; put it upstairs
	move.w	randomseed(a6),d1	; load second random seed
	move.w	d1,random2seed(a6)	; put it in first random seed
	swap.w	d1			; swap again
	lsr.l	#1,d1			; *2
	eor.w	d1,randomseed(a6)	; eor it
	move.w	randomseed(a6),d1	; move it in
	andi.l	#32767,d1		; and it (make it int)
	divu.w	d0,d1			; divide
	swap.w	d1			; swap again
	addq.w	#1,d1			; add 1
	move.w	d1,d0			; prepare to push
	bra	pushword		; push it on stack

*--- bytecode JLop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: ``if p1 less than p2''

JLop
	cmp.w	d1,d0	; if p1 < p2
	blt	jumpt	; T
	bra	jumpf	; F

*--- bytecode JGop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: ``if p1 greater than p2''

JGop  cmp.w   d1,d0   ; if p1 > p2
	bgt	jumpt	; T
	bra	jumpf	; F

*--- bytecode TESTop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: ``p1 & p2 == p1''

TESTop
	not.w	d0	; negate p1
	and.w	d0,d1	; AND p2
	beq	jumpt	; T
	bra	jumpf	; F

*--- bytecode ORop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: ``p1 OR p2 -> (ind)''

ORop
	or.w	d1,d0		; p1 OR p2
	bra	pushword	; push d0 into indirection in next bytecode

*--- bytecode NOTop ---*
* parameters: d0: p1
* returns: nothing
* side effects: negates p1 and pushes it into indirection


NOTop
	not.w	d0
	bra	pushword

*--- bytecode ANDop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: ``p1 AND p2 -> (ind)''

ANDop
	and.w	d1,d0		; p1 AND p2
	bra	pushword	; push into indirection

*--- bytecode JEop ---*
* parameters: a0 ptr to parameter array
* returns: goes to jumpt if parameter 1 equivalent to any subsequent one.
*	   goes to jumpf of no equivalence was found.
* side effects: tests equivalence of parameter 1 to subsequent ones.

JEop
	nop			; nop flags ``parameters in array''
	move.w	(a0)+,d1	; number of parameters
	move.w	(a0)+,d0	; first parameter
	subq.w	#1,d1		; popped it off.
1$	cmp.w	(a0)+,d0	; compare with next parameter to first one
	beq.s	2$		; equal -> jumpt
	subq.w	#1,d1		; popped off one more
	bne.s	1$		; any more parameters?
	bra	jumpf		; no parameter equal to parameter 1
2$	bra	jumpt		; equivalence!

*--- bytecode JZop ---*
* parameters: d0: p1
* returns: nothing
* side effects: if parameter = 0

JZop
	tst.w	d0
	beq	jumpt		; T
	bra	jumpf		; F

*--- objaddr ---*
* parameters: d0: object number (1 ... 255)
* returns: a0: address of object table entry
* There is no object 0. The first 62 bytes are the property defaults.

objaddr
	mulu.w	#9,d0		; d0 * 9
	movea.l obj_tab(a6),a0  ; obj_tab
	adda.l	d0,a0		; +
	add	#53,a0		; +53 bytes (62-9)
	rts

*--- plist_addr ---*
* parameters:	 a0 - object address	
* returns:	 a0 - address of property list
* side effects

plist_addr
	add	#7,a0		; properties field of object struct
	moveq.l #0,d0		; safety measure
	bsr	getint		; get byte address of property table
	movea.l blockp(a6),a0	; compute address of properties table
	adda.l	d0,a0		; via the byte address
	clr.w	d0		; safety measure
	move.b	(a0)+,d0	; get text length of object name
	add.w	d0,d0		; its the length in words
	add	d0,a0		; skip the text
	rts			; return the actual property list!

;;; next_prop - get the next property
;;; parameters:	a0 - points at current property
;;; returns:	a0 - points at next property

next_prop
	clr.w	d0		; safety measure
	move.b	(a0)+,d0	; get size byte
	asr.w	#5,d0		; bottom 5 bits are prop #, trash them
	addq.w	#1,d0		; top three are length - 1
	add	d0,a0		; a0 now points at next property
	rts

*--- bytecode INSERT_OBJop ---*

INSERT_OBJop
	movem.w d0-d1,-(a7)	; save object numbers for further use
	bsr.s	REMOVE_OBJop	; remove any references from parents, siblings
	move.w	2(a7),d0	; number of object to insert
	bsr.s	objaddr		; compute its address
	movea.l a0,a1		; stash it
	move.w	(a7),d0		; number of second object
	bsr.s	objaddr		; compute its address
	movem.w (a7)+,d0-d1	; restore object numbers
	move.b	6(a1),5(a0)	; set sibling of object
	move.b	d1,4(a0)	; parent
	move.b	d0,6(a1)	; set new child of parent
	rts

*--- bytecode REMOVE_OBJop ---*

REMOVE_OBJop
	move.w	d0,d1		; save object number for further use
	bsr.s	objaddr		; get object address
	movea.l a0,a1		; stash it away
	move.b	4(a1),d0	; get parent object number
	beq.s	3$		; no parent -> return
	andi.w	#$FF,d0		; safety measure
	bsr.s	objaddr		; get parent object address
	move.b	6(a0),d0	; get number of first child of parent
	cmp.b	d0,d1		; are we the first child
	bne.s	1$		; no
	move.b	5(a1),6(a0)	; yes, put our sibling into parent's child
	bra.s	2$		; clean up out parent and sibling fields
1$	andi.w	#$FF,d0		; safety measure
	bsr	objaddr		; get address next child in list
	move.b	5(a0),d0	; get its sibling
	cmp.b	d0,d1		; are we it?
	bne.s	1$		; no, try next
	move.b	5(a1),5(a0)	; yes, let its sibling be out sibling
2$	clr.b	4(a1)		; make us an orphan ...
	clr.b	5(a1)		; ... without siblings
3$	rts

*--- bytecode GET_PARENTop ---*

GET_PARENTop
	bsr	objaddr		; get object address
	clr.w	d0		; safety
	move.b	4(a0),d0	; get parent number
	bra	pushword	; store it

*--- bytecode GET_CHILDop ---*

GET_CHILDop
	bsr	objaddr		; get object's address
	clr.w	d0		; dafety
	move.b	6(a0),d0	; get child number
	move.w	d0,-(a7)	; save it
	bsr	pushword	; store it
	tst.w	(a7)+		; restore and test it
	bne	jumpt		; tadiii
	bra	jumpf		; tadaaa

*--- bytecode GET_SIBLINGop---*

GET_SIBLINGop
	bsr	objaddr		; get the object's address
	clr.w	d0		; safety measure
	move.b	5(a0),d0	; get sibling number
	move.w	d0,-(a7)	; save it for later
	bsr	pushword	; store it
	tst.w	(a7)+		; it got clobbered
	bne	jumpt		; and jump if not 0
	bra	jumpf		; or else, jump if 0

*--- bytecode JINop ---*
* parameters: d0: reference
*	      d1: byte to compare
* returns: nothing
* side effects: jumps if the byte is equal to whatever we computed

JINop
	bsr	objaddr
	cmp.b	4(a0),d1
	beq	jumpt
	bra	jumpf

;;; GET_PROPop - get an object's property, or the default.
	
GET_PROPop
	bsr	objaddr		; get the object address
	bsr	plist_addr	; so we can get the p-list address
	bra.s	2$		; the first property is already loaded
1$	bsr	next_prop	; get address of next property
2$	move.b	(a0),d0		; property size byte
	andi.w	#$1F,d0		; low 5 bits are property number
	cmp.w	d1,d0		; is it the property we are looking for?
	bgt.s	1$		; no, but there still is hope
	blt.s	3$		; no, and it's not in this table, either
	btst.b	#5,(a0)+	; is it a length-1 (or odd-length) property?
	bne.s	4$		; no
	move.b	(a0),d0		; yes, get first byte of property data
	bra	pushbyte	; store it, and over
3$	subq.w	#1,d1		; subtract 1 from property number
	add.w	d1,d1		; and multiply it, so we can look it up
	movea.l obj_tab(a6),a0	; in the property defaults table
	add	d1,a0           ; get the default address
4$	bsr	getint		; fetch default
	bra	pushword	; store it, and over.

;;; PUT_PROPop - writes value to property of object
	
PUT_PROPop
	bsr	objaddr		; get object's addres
	bsr	plist_addr	; and with it, the p-lists address
	bra.s	2$		; and we already are at the first property
1$	bsr	next_prop	; get next property
2$	move.b	(a0),d0		; get size byte
	andi.w	#$1F,d0		; extract property number
	cmp.w	d1,d0		; is it the one we are looking for?
	bgt.s	1$		; no, check next one
	beq.s	3$		; yes, write value into it
	clr.w	d0		; wait a minute, WHICH property?
	lea.l	noputp,a0	; "non-existant put property"
	bra	internal_error	; complain
3$	btst.b	#5,(a0)+	; is the property length odd?
	bne.s	4$		; no (since the length bits are l-1)
	move.b	d2,(a0)		; yes, store only a byte
	rts			; and return
4$	move.w	d2,d0		; prepare to store the word
	bra	moveword	; and do it

;;; GET_NEXT_PROPop - get the next property

GET_NEXT_PROPop
	bsr	objaddr		; get object's ...
	bsr	plist_addr	; ... p-list
	tst.w	d1		; operand 0?
	beq.s	4$		; yes, get first property number present
	bra.s	2$		; else, we already have the first property
1$	bsr	next_prop	; get next property
2$	move.b	(a0),d0		; get size byte
	andi.w	#$1F,d0		; extract property number
	cmp.w	d1,d0		; is it out property?
	bgt.s	1$		; no, try next one
	beq.s	3$		; yes, get next property and return its #
	clr.w	d0		; no, and it's not going to show up!
	lea.l	nonext,a0	; complain about `non-existant next property'
	bra	internal_error	; do it
3$	bsr	next_prop	; get the next property, so we can return its #
4$	move.b	(a0),d0		; get its size byte
	andi.w	#$1F,d0		; mask out size bits
	bra	pushword	; and store the property number.

*--- attr_info ---*
* parameters:  a0 - the objects address
*              d0 - the attribute number
* returns:     a0 - addtess of the byte with the attribute bit in it
*              d0 - the number of the attribute bit in that byte
* side effects get position of an objects attribute

attr_info
 	addq.l	#1,a0
	subq.w	#8,d0
	bge.s	attr_info
	subq.l	#1,a0
	neg.w	d0
	subq.w	#1,d0
	rts

*--- bytecode TEST_ATTRop ---*

TEST_ATTRop
	bsr	objaddr
	move.w	d1,d0
	bsr.s	attr_info
	btst.b	d0,(a0)
	bne	jumpt
	bra	jumpf

*--- bytecode SET_ATTRop ---*

SET_ATTRop
	bsr	objaddr
	move.w	d1,d0
	bsr.s	attr_info
	bset.b	d0,(a0)
	rts

*--- bytecode CLEAR_ATTRop ---*

CLEAR_ATTRop
	bsr	objaddr
	move.w	d1,d0
	bsr.s	attr_info
	bclr.b	d0,(a0)
	rts

*--- bytecode LOADWop---*
* parameters: d0: p1 block number
*	      d1: p2 word number
* returns: nothing
* side effects: loads a word from the file into indirection

LOADWop
	asl.w	#1,d1		; p2 *= 2
	add.w	d1,d0		; p1 += p2
	bsr	badr2adr	; compute block number & id
	bsr	fetchint	; get the int (in d2)
	move.w	d2,d0		; put it into d0
	bra	pushword	; push it into indirection at current BC

*--- bytecode LOADBop ---*
* parameters: d0: p1
*	      d1: p2
* returns: nothing
* side effects: loads byte from file into indirection

LOADBop
	add.w	d1,d0
	bsr	badr2adr
	bsr	fetchbyte
	move.w	d2,d0
	bra	pushbyte

;;; STOREWop - store a word in a word table
;;; parameters:	d0 - byte address of table
;;;             d1 - table index
;;;             d2 - value
	
STOREWop
	asl.w	#1,d1		; p2 *= 2
	add.w	d1,d0		; + p1
	movea.w d0,a0		; put it here
	adda.l	blockp(a6),a0	; + file base
	move.w	d2,d0		; p3
	bra	moveword	; put p3 here

*--- bytecode STOREBop ---*
* parameters: d0: array address
*	      d1: byte array index
*	      d2: byte to copy
* returns: nothing
* side effects: writes a byte into a byte array

STOREBop
	add.w	d1,d0
	movea.l blockp(a6),a0
	move.b	d2,0(a0,d0.w)
	rts

*--- bytecode GET_PROP_ADDRop ---*

GET_PROP_ADDRop
	bsr	objaddr		; get object address
	bsr	plist_addr	; so we get the p-list address
	bra.s	2$		; and we already havethe first property here.
1$	bsr	next_prop	; get next property
2$	move.b	(a0),d0		; get size byte
	andi.w	#$1F,d0		; extract property number
	cmp.w	d1,d0		; is it our property?
	bgt.s	1$		; no, but still hope
	beq.s	3$		; yes, return its address
	clr.w	d0		; no, so return 0
	bra.s	4$		; and push it
3$	addq.l	#1,a0		; skip size byte
	move.l	a0,d0		; and convert it to a byte address
	sub.l	blockp(a6),d0	; in z-memory
4$	bra	pushword	; and store it.

*--- bytecode GET_PROP_LENop ---*
* parameter: d0: byte address of property
* returns: nothing
* side effects:

GET_PROP_LENop
	movea.l blockp(a6),a0	; convert byte address ...
	add	d0,a0		; ... to address
	clr.w	d0		; clear
	move.b	-1(a0),d0	; get size byte of property
	asr.w	#5,d0		; get at the size bits
	addq.w	#1,d0		; +1
	bra	pushword	; stick it in stack or wherever

*--- bytecode ILLEGALop14 ---*

ILLEGALop14
	bra	ILLEGALop

;;; LOADop - store the value of one variable into another.
;;; parameters:	d0 - number of variable to load
	
LOADop
	bsr	rclint
	bra	pushword

*--- bytecode STOREop ---*
* parameters: d0: indirection
*	      d1: thing to store
* returns: nothing
* side effects:

STOREop
	bra	storeint	; store the thing

*--- bytecode PUSHop ---*
* parameters: d0: a word
* returns: nothing
* side effects: pushes word on stack

PUSHop
	move.w	d0,-(a4)	; push the word
	rts			; return

*--- bytecode PULLop ---*
* parameters: do: indirection
* returns: nothing
* side effects: pops something off the stack and stores it

PULLop
	move.w	(a4)+,d1
	bra	storeint

*--- bytecode INCop ---*
* parameters: d0: p1 indirection
* returns: nothing
* side effects: increases thing in indirection

INCop
	move.w	d0,d1
	bsr	rclint
	addq.w	#1,d0
	exg.l	d0,d1
	bra	storeint

*--- bytecode DECop ---*
* parameters: d0: p1
* returns: nothing
* side effects: subtracts one from thing in indirection

DECop
	move.w	d0,d1		; save indirection for later
	bsr	rclint		; recall thing into d0
	subq.w	#1,d0		; decrease it
	exg.l	d0,d1		; switch registers
	bra	storeint	; store it

*--- bytecode INC_CHKop ---*
* parameters: d0: p1 (indirection)
*		  0	: top of stack
*		  1...15: etc
*		  16... : and so on
*	      d1: p2 compare with
* returns: nothing
* side effects: ``increment and jump if greater than''

INC_CHKop
	move.w	d1,d2		; save p2
	move.w	d0,d1		; save p1
	bsr	rclint		; recall word according to p1
	addq.w	#1,d0		; plus 1
	exg.l	d0,d1		; switch registers
	cmp.w	d2,d1		; incremented word > p2?
	bgt.s	LLL_1		; yes
LLL_3	bsr	storeint	; store stack+1 according to p1
	bra	jumpf		; F
LLL_1	bsr	storeint	; store stack+1 according to p1
	bra	jumpt		; T

*--- bytecode DEC_CHKop ---*
* parameters: d0: p1 indirection
*	      d1: p2 compare with
* returns: nothing
* side effects: ``decrement and jump if less than''

DEC_CHKop
	move.w	d1,d2		; save d1
	move.w	d0,d1		; save d0
	bsr	rclint		; recall word
	subq.w	#1,d0		; decrement recalled word
	exg.l	d0,d1		; d0: indirection; d1: word.
	cmp.w	d2,d1		; decremented word < p2 ?
	blt.s	LLL_1		; T
	bra.s	LLL_3		; F

;;; SREADop - read a whole command from the keyboard

SREADop
	nop			; flags ``parameters in array, please''
;;; compute address of input buffer
	moveq.l #0,d0		; clear d0
	move.w	2(a0),d0	; `text' buffer to store the input in
	add.l	blockp(a6),d0	; address of text buffer
	move.l	d0,sread_text(a6) ; sread_text now is that place in the story
;;; compute address of parse buffer
	moveq.l #0,d1		; clear d1
	move.w	4(a0),d1	; `parse' buffer for lexical analysis
	add.l	blockp(a6),d1	; address of parse buffer
	move.l	d1,sread_parse(a6) ; sread_parse now is that place in the story
;;; update screen
	bsr	SHOW_STATUSop	; print title and score or time
	bsr	flush_con_buf   ; print prompt
;;; read input line
	movea.l sread_text(a6),a0
	bsr	get_input_line	; get input
	move.l	a0,eoinput(a6)	; eoinput points end of input
;;; END OF INPUT PHASE

	move.l	a4,-(a7)	; save a4
	movea.l sread_text(a6),a2
	addq.l	#1,a2		; first input byte
	movea.l sread_parse(a6),a4
	addq.l	#2,a4		; first lexer byte
	clr.b	nwords(a6)	; clear nwords

	;; this prepares to cut out an input word
1$	lea.l	wordbuf(a6),a3	; make a3 point to wordbuf (buffer)
	move.l	a2,bofword(a6)	; remember beginning of word
	;; this loop cuts out a word from the input
2$	cmpa.l	eoinput(a6),a2	; is (a2) end of input?
	bne.s	3$		; no? goto 3$
	lea.l	wordbuf(a6),a0	; else, make a0 point to wordbuf (buffer)
	cmpa.l	a3,a0		; any characters considered yet?
	beq	9$		; no, goto 9$ (over and out)
	bra.s	7$		; yes, goto 7$ (lex the character)
	;; this loop compares an input byte with the keyboard input table
3$	move.b	(a2)+,d0	; get byte from typed input
	movea.l kbincod(a6),a1	; get keyboard input codes
4$	cmp.b	(a1)+,d0	; is it one?
	beq.s	5$		; yes, goto 5$
	tst.b	(a1)		; end of keyboard input codes?
	bne.s	4$		; no, try next
	;; end of keyboard input table loop
	;; we get here if the character wasn't a separator code
	lea.l	wordbuf(a6),a0	; a0 is beginning of buffer
	add	#6,a0		; advance by 6 characters (end of buffer)
	cmpa.l	a0,a3		; there yet?
	beq.s	2$		; yes, goto 2$ (buffer full, don'd add char)
	move.b	d0,(a3)+	; else, copy byte to (a3)
	bra.s	2$		; goto 2$

	;; end of loop
	;; here, we found a separator code, and if it wasn't the first
	;; character of a word, we unread it, else if it was an `ignore'
	;; character, we ignore it, else we lex the word.
5$	lea.l	wordbuf(a6),a0	; buffer
	cmpa.l	a3,a0		; buffer position
	bne.s	6$		; not first buffer position - goto 6$
	cmpa.l	kbinclim(a6),a1	; is it an `ignore separator'?
	bhi.s	1$		; yes, well ignore it and get next word
	move.b	d0,(a3)+	; no, put it into buffer
	bra.s	7$		; prepare to lex our word
6$	subq.l	#1,a2		; unread last character

	;; here, we look whether there is room for our lexed word
7$	addq.b	#1,nwords(a6)	; we lexed another word, keep score
	move.b	nwords(a6),d0	; so...
	movea.l sread_parse(a6),a0 ; check whether we hit the limit
	cmp.b	(a0),d0		; of the lexer buffer
	ble.s	8$		; not yet, goto 8$
	bsr	newline		; yes, complain
	lea.l	manyword,a0	; about too many words
	bsr	printcrstring	; print complaint
	clr.b	nwords(a6)	; no words lexed
	bra.s	9$		; over and out

	;; here, we fill out the 4-byte field for a lexed word
8$	move.l	a2,d0		; input position
	sub.l	bofword(a6),d0	; - beginning of word
	move.b	d0,2(a4)	; put length of word into length field of lexer

	move.l	bofword(a6),d0	; beginning of word
	sub.l	sread_text(a6),d0 ; - beginning of text
	move.b	d0,3(a4)	; put positon of word into lexer position field

	clr.b	(a3)		; mark end of buffer

	lea.l	wordbuf(a6),a1	; load buffer into a1
	lea.l	dicwordbuf(a6),a0 ; load dictionary format buffer
	bsr	word2dict	; convert the word to dictionary format

	lea.l	dicwordbuf(a6),a0 ; word in dictionary format
	bsr	findict		; find word in dictionary
	movea.l a4,a0		; put address of lexer field here
	bsr	moveword	; write address of dictionary word into lexer
	addq.l	#4,a4		; advance to next lexer field
	bra	1$		; process next word
	;; this is the exit - we write the number of lexed words and return.
9$	movea.l sread_parse(a6),a0 ; make a0 point to lexer buffer
	move.b	nwords(a6),1(a0) ; put number of words into byte 1 of lexer
	movea.l (a7)+,a4	; restore a4
	rts			; return

;;; get_input_line
;;; parameters: a0: point to buffer where input is put.
;;; first byte in a0 is buffer length
;;; input goes into second byte onward.
;;; returns: a0: first byte of input
;;;          d0: byte beyond input

get_input_line
	movem.l d3/a1-a3,-(a7)	; rescue registers
	clr.w	inbufcnt(a6)	; clear inbufcnt
	clr.w	d3		; clear `buffer full' flag
	clr.w	d0		; clear
	move.b	(a0)+,d0	; first byte of text buffer is buffer length
	movea.l a0,a1		; a1 is pointer to buffer space
	movea.l a1,a2		; copy
	movea.l a2,a3		; copy
	add	d0,a3		; a3 points to end of buffer
1$	bsr	get_char_echo1	; get a character, echo to screen
	cmpi.b	#13,d0		; CR? goto 5$
	beq.s	5$
	cmpi.b	#8,d0		; BS?
	bne.s	2$		; no, skip to 2$
	cmpa.l	a1,a2		; are we at beginning?
	beq.s	1$		; yes, forget it, next input
	subq.l	#1,a2		; no, delete character
	bra.s	1$		; next input
2$	cmpi.b	#$41,d0		; 'A'?
	blt.s	3$		; below, skip to 3$
	cmpi.b	#$5A,d0		; 'Z'?
	bgt.s	3$		; above, skip to 3$
	addi.b	#$20,d0		; else, convert to lower case
3$	cmpa.l	a3,a2		; end?
	beq.s	4$		; yes, skip to 4$
	move.b	d0,(a2)+	; no, append to buffer
	bra.s	1$		; get next input
4$	moveq.l #1,d3		; flag `buffer full'
5$	movea.l a1,a0		; first
	move.l	a2,d0		; last
	bsr	lprint_input	; dump input to printer
	tst.w	d3		; buffer full?
	beq.s	6$		; no, skip to 6$
	bsr	newline		; yes, print a new-line
	bsr	newline		; another
	lea.l	longcmd,A0	; complain about a long command
	bsr	printcrstring	; print complaint
	movea.l a1,a2		; restore
6$	movea.l a2,a0		; these
	movem.l (a7)+,d3/a1-a3	; and these
	rts			; done.

;;; findict - find word in dictionary
;;; parameters:	a0 - word in dictionary format
;;; returns:	d0 - address of dictionary word
	
findict
	movem.l d1/a1-a2,-(a7)	; save these
	move.w	ndentry(a6),d0	; d0 - number of entries in dictionary
	move.w	dentlen(a6),d1	; d1 - entry length
	asr.w	#1,d0		; nof entries /= 2
1$	asl.w	#1,d1		; entry length *= 2
	asr.w	#1,d0		; nof entries /= 2
	bne.s	1$		; not 0?
	movea.l a0,a1		; get word for find
	movea.l dictdat(a6),a2
	add	d1,a2
	sub	dentlen(a6),a2
2$	asr.w	#1,d1
	movea.l a2,a0
	bsr	getint
	cmp.w	(a1),d0
	bcs.s	3$
	bhi.s	4$
	bsr	getint
	cmp.w	2(a1),d0
	bcs.s	3$
	bhi.s	4$
	move.l	a2,d0
	sub.l	blockp(a6),d0
	bra.s	6$		; found - return
3$	add	d1,a2
	cmpa.l	lastdic(a6),a2
	bls.s	5$
	movea.l lastdic(a6),a2
	bra.s	5$
4$	sub	d1,a2
5$	cmp.w	dentlen(a6),d1
	bge.s	2$
	clr.w	d0
6$	movem.l (a7)+,d1/a1-a2
	rts

*--- char2mystring ---*
* parameters: d0  a char
* returns: nothing
* side effects: appends the char to mystring(a6). (no zero terminator)

char2mystring
	movea.l mystring(a6),a0
	move.b	d0,(a0)+
	move.l	a0,mystring(a6)
	rts

*--- str2mystring ---*
* parameters:
* returns:
* side effects:

LLL36
	bsr.s	char2mystring
str2mystring
	move.b	(a1)+,d0
	bne.s	LLL36
	rts

*--- bytecode SHOW_STATUSop ---*
* parameters: none
* returns: nothing
* side effects: puts up current titlebar

SHOW_STATUSop
	move.w	#1,mystrflag(a6)	; let's print into ``mystring''
	sub	#256,a7			; make room for a title bar
	move.l	a7,l6(a6)		; put title bar buffer here
	move.l	a7,mystring(a6)		; and here
	move.b	#32,d0			; initial space
	bsr.s	char2mystring		; into the titlebar
	moveq.l #16,d0			; memory register 16 is room name
	bsr	rclint			; get room name string address
	bsr	PRINT_OBJop             ; print room name
	move.l	mystring(a6),l7(a6)	; save title bar
	tst.w	s_t_flag(a6)		; is this a ``time'' game?
	bne.s	1$			; yes
	lea.l	scores,a1		; "Score  "
	bsr.s	str2mystring		; put the word into the title bar
	moveq.l #17,d0			; memory register 17 is score
	bsr	rclint			; get the score
	bsr	PRINT_NUMop		; print the score
	move.b	#47,d0			; print a slash
	bsr.s	char2mystring		; into mystring
	moveq.l #18,d0			; memory register 18 is move
	bsr	rclint			; get the move number
	bsr	PRINT_NUMop		; print the move
	bra.s	5$			; done.
1$	lea.l	times,a1		; "Time	  "
	bsr.s	str2mystring
	moveq.l #17,d0
	bsr	rclint
	move.b	#97,d2
	cmpi.w	#12,d0
	blt.s	3$
	beq.s	2$
	subi.w	#12,d0
2$	move.b	#112,d2
3$	tst.w	d0
	bne.s	4$
	moveq.l #12,d0
4$	move.w	d2,-(a7)
	bsr.s	PRINT_NUMop
	move.b	#58,d0
	bsr	char2mystring
	moveq.l #18,d0
	bsr	rclint
	ext.l	d0
	divu.w	#10,d0
	move.l	d0,-(a7)
	addi.b	#48,d0
	bsr	char2mystring
	move.l	(a7)+,d0
	swap.w	d0
	addi.b	#48,d0
	bsr	char2mystring
	moveq.l #32,d0
	bsr	char2mystring
	move.w	(a7)+,d0
	bsr	char2mystring
	move.b	#109,d0
	bsr	char2mystring

5$	move.b	#32,d0			; print a space
	bsr	char2mystring		; into mystring
	movea.l l6(a6),a0		; room name string
	movea.l l7(a6),a1		; score/time string
	move.l	a1,d0			; end of room name
	move.l	mystring(a6),d1		; end of score/time
	sub.l	a0,d0			; length of room name
	sub.l	a1,d1			; length of score/time
	bsr	printtitle		; print the titlebar
	add	#256,a7			; restore stack
	clr.w	mystrflag(a6)		; let's use the regular buffer
	rts				; done.

*--- bytecode PRINT_CHARop ---*
* parameters: d0: a char
* returns: nothing
* side effects: prints the char


PRINT_CHARop
	bra	outputchar

*--- bytecode PRINT_NUMop ---*
* parameters: d0  an int
* returns: nothing
* side effects: prints the int in decimal notation

PRINT_NUMop
	move.w	d0,d1
	bne.s	1$
	move.b	#48,d0		; ascii for '0'
	bra	outputchar	; print a 0
1$	bgt.s	2$		; positive number?
	move.b	#45,d0		; ascii for '-'
	bsr	outputchar	; print a -
	neg.w	d1		; flip sign of parameter
2$	clr.w	d2		; clear digit counter
	bra.s	4$
3$	ext.l	d1
	divu.w	#10,d1		; divide by 10
	swap.w	d1		; get remainder
	move.w	d1,-(a7)	; push remainder (decimal digit) on stack
	swap.w	d1		; get quotient
	addq.w	#1,d2		; increase digit counter by 1
4$	cmpi.w	#10,d1
	bge.s	3$		; already split up into digits?
	move.w	d1,d0
	bra.s	6$
5$	move.w	(a7)+,d0	; fetch digit
6$	addi.b	#48,d0		; print decimal digit in d0  add ascii for '0'
	bsr	outputchar	; print digit
	subq.w	#1,d2		; next digit
	bge.s	5$
	rts

;;; PRINT_PADDRop - print string at packed address
;;; parameters:	do0 - packed address of string
	
PRINT_PADDRop
	bsr	padr2adr	; compute segment address
	bra	prndecrypt	; decypher and print the string

;;; PRINT_ADDRop - print z-string at byte address
;;; parameters:	d0 - byte address of string
	
PRINT_ADDRop
	bsr	badr2adr	; compute segment address
	bra	prndecrypt	; print the string

;;; PRINT_OBJop - print the `short name' of an object
;;; parameters:	d0 - object number

PRINT_OBJop
	bsr	objaddr		; get the object address
	add	#7,a0		; skip to the properties
	bsr	getint		; get byte address of properties
	addq.w	#1,d0		; skip length byte
	bsr	badr2adr	; compute segment and index
	bsr	prndecrypt	; decrypt and print the string
	rts

;;; PRINTop - print z-string starting right after the opcode

PRINTop
	move.w	block_number(a6),d0	; load params for prndecrypt
	move.w	block_index(a6),d1
	bsr	prndecrypt		; prndecrypt
	move.w	d0,block_number(a6)	; update PC
	move.w	d1,block_index(a6)
	bra	swapblock		; refresh memory

;;; PRINT_RETop - print the z-string starting right after the
;;; bytecode, then print a new line, then return TRUE (1) from the
;;; current subroutine.

PRINT_RETop
	bsr.s	PRINTop		; print the string
	bsr.s	NEW_LINEop	; print new-line
	bra	RTRUEop		; return TRUE

*--- bytecode NEW_LINEop ---*
* parameters: none
* returns: nothing
* side effects: prints a newline on the screen

NEW_LINEop
	bra	newline		; print a new line

*--- bytecode SPLIT_WINDOWop ---*
* parameters: d0: new top line, 0 = the one below title bar
* more clearls said: d0 is the new size of the upper window.
* returns: nothing
* side effects: moves the upper border for the scrolling text up or down.

SPLIT_WINDOWop
	move.w	text_top,d1	; get top text line (excluding title bar)
	addq.w	#1,d0		; p1 += 1
	move.w	d0,text_top	; new text_top
	cmp.w	d1,d0		; copare old & new text top
	ble.s	1$		; new one further up or same -> return
	move.w	d0,-(a7)	; else, clear the lines from the new top
	move.w	d1,-(a7)	; to the old top
	jsr	clearlin	; (in C part)
	addq.l	#4,a7		; correct stack
1$	rts			; and return

*--- bytecode SET_WINDOWop ---*
* parameters: d0: 0: select upper window, turn scripting off
*		  1: select lower window, turn scripting on
* returns: nothing
* side effects: toggles the SCRIPT switch

SET_WINDOWop
	tst.w	d0			; d0 = 0?
	beq.s	1$
	cmpi.w	#1,d0			; d0 = 1?
	beq.s	3$
	rts				; anything else is ignored
;;; this is the entry point when the lower window is selected
1$	tst.w	sel_wind(a6)		; sel_wind = 0?
	beq.s	2$			; yes, return
	clr.w	sel_wind(a6)		; no, clear it
	move.w	old_line(a6),crsr_lin	; restore cursor position
	move.w	old_col(a6),crsr_col
2$	rts				; return
;;; this is the entry point when the upper window is selected
3$	tst.w	sel_wind(a6)		; sel_wind = 1 already?
	bne.s	4$			; yes, reset cursor and return
	move.w	#1,sel_wind(a6)		; no, set it
	move.w	crsr_lin,old_line(a6)	; save cursor position
	move.w	crsr_col,old_col(a6)	; yes, both
4$	clr.w	crsr_lin		; reset cursor
	clr.w	crsr_col
	addq.w	#1,crsr_lin		; title bar is off limits
	rts				; return

*--- unused code ---*
*
* 0B03A0	  BSR	    $B0C1C
* 0B03A4	  MOVE.L    D0,$FF32(A6)
* 0B03A8	  RTS
* 0B03AA	  BSR	    $B0C1C
* 0B03AE	  SUB.L	    $FF32(A6),D0
* 0B03B2	  DIVU.W    #$6,D0
* 0B03B6	  SUB.W	    $FF38(A6),D0
* 0B03BA	  NEG.W	    D0
* 0B03BC	  RTS

*--- bytecodes ILLEGALop4 to ILLEGALop13 ---*

ILLEGALop4  bra	    ILLEGALop

ILLEGALop5  bra	    ILLEGALop

ILLEGALop6  bra	    ILLEGALop

ILLEGALop7  bra	    ILLEGALop

ILLEGALop8  bra	    ILLEGALop

ILLEGALop9  bra	    ILLEGALop

ILLEGALop10	    bra	    ILLEGALop ; OUTPUT_STREAM

ILLEGALop11	    bra	    ILLEGALop ; INPUT_STREAM

ILLEGALop12	    bra	    ILLEGALop ; SOUND_EFFECT

ILLEGALop13	    bra	    ILLEGALop

*--- bytecode ILLEGALop2 ---*

ILLEGALop2
	bra	ILLEGALop

*--- bytecode ILLEGALop1 ---*

ILLEGALop1
	bra	ILLEGALop

*--- bytecode ILLEGALop3 ---*

ILLEGALop3
	bra	ILLEGALop

;;; CALLop - call routine with 0, 1, 2, or 3 parameters and store the
;;; resulting return value

CALLop
	nop				; flags parameter passing
	move.w	(a0)+,d2		; number of params
	move.w	(a0)+,d0		; get call address
	bne.s	1$			; call address 0
	clr.w	d0			; return 0
	bra	pushword		; pushes zero
1$	movea.l a0,a1			; params
	subq.w	#1,d2			; #params-1
	move.w	block_number(a6),-(a4)	; block number of pc on stack
	move.w	block_index(a6),-(a4)	; block index of pc on stack
	move.l	currframe(a6),d1	; current stack frame
	sub.l	stackbot(a6),d1		; -bottom of stack space=offset
	move.l	d1,-(a4)		; push return frame
	bsr	padr2adr		; comvert p1 to new pc
	move.w	d0,block_number(a6)	; load new pc block_number
	move.w	d1,block_index(a6)	; load new pc block_index
	bsr	swapblock		; refresh memory
	move.l	a4,currframe(a6)	; make new frame pointer
	subq.l	#2,currframe(a6)	; adjust current frame
	bsr	fetch_bytecode		; fetch number of subroutine params
	move.w	d0,d1			; save it
	beq.s	4$			; no parameters to pass.
2$	bsr	fetch16bit		; fetch a parameter
	subq.w	#1,d2			; params - 1
	blt.s	3$			; no more?
	move.w	(a1)+,d0		; use param from array
3$	move.w	d0,-(a4)		; put param on stack
	subq.w	#1,d1			; any more subroutine params?
	bgt.s	2$			; yes
4$	rts				; return

;;; RETop - return from subroutine with return value
	
RETop
	movea.l currframe(a6),a4	; load currframe
					; into bytecode stack pointer
	addq.l	#2,a4			; fp points at first local
	move.l	(a4)+,d1		; previous stack frame offset
	add.l	stackbot(a6),d1		; base of stack space
	move.l	d1,currframe(a6)	; previous stack frame
	move.w	(a4)+,block_index(a6)	; pop off pc block_index
	move.w	(a4)+,block_number(a6)	; pop off pc block_number
	move.w	d0,d1			; save parameter into d1
	bsr	swapblock		; refresh memory
	move.w	d1,d0			; restore parameter from d1 into d0
	bra	pushword		; go to pushword

;;; RFALSEop - return from subroutine with return value 1
	
RTRUEop
	moveq.l #1,d0		; return value 1
	bra.s	RETop

;;; RFALSEop - return from subroutine with return value 0

RFALSEop
	clr.w	d0		; return value 0
	bra.s	RETop

;;; JUMPop - jump unconditionally
;;; parameters:	d0 - 16-bit signed offset to apply to pc, after subtracting 2
	
JUMPop
	subq.w	#2,d0		; in the branch formula, this makes sense
	add.w	d0,block_index(a6) ; frob the pc
	bra	swapblock	; refresh virtual memory

;;; RET_POPPEDop - pop top of stack and return that
	
RET_POPPEDop
	move.w	(a4)+,d0
	bra.s	RETop

*--- bytecode POPop ---*

POPop	addq.l	#2,a4	; pop something off stack without regarding it
	rts

;;; NOP-op - the ``official'' NO-OPERATION opcode.
	
NOPop
	rts

;;; SAVEop - attempt to save the game, and branch if successful

SAVEop
	clr.w	d0		; prepare to close the story file
	bsr	storyopcl	; close story file
	bsr	ask_save_fname	; get file name and open file
	bne.s	3$		; 
	bsr	create_save_file ; create the save file
	bne.s	3$		; didn't work? oh, well
	move.w	block_number(a6),-(a4)
	move.w	block_index(a6),-(a4)
	move.l	currframe(a6),-(a4)
	move.l	stackbot(a6),d0
	sub.l	d0,(a4)
	move.w	relno(a6),-(a4)
	movea.l d0,a0
	move.l	a4,(a0)
	sub.l	d0,(a0)
	movea.l stackbot(a6),a0
	clr.w	d0
	moveq.l #1,d1
	bsr	LBL74
	bne.s	1$
	add	#10,a4
	movea.l blockp(a6),a0
	moveq.l #1,d0
	move.w	statmem(a6),d1
	bsr	LBL74
	bne.s	1$
	bsr	LBL75
	bne.s	2$
	bsr	LBL76
	moveq.l #1,d0
	bsr	storyopcl
	bra	jumpt
1$	bsr	LBL75
2$	bsr	LBL77
3$	bsr	LBL78
	moveq.l #1,d0
	bsr	storyopcl
	bra	jumpf

*--- bytecode RESTOREop ---*

RESTOREop
	clr.w	d0
	bsr	storyopcl
	bsr	LBL79
	bne.s	1$
	bsr	LBL80
	bne.s	1$
	clr.w	d0
	moveq.l #1,d1
	movea.l stackbot(a6),a0
	bsr	LBL81
	bne.s	3$
	movea.l stackbot(a6),a0
	movea.l (a0),a4
	adda.l	a0,a4
	move.w	(a4)+,d0
	cmp.w	relno(a6),d0
	bne.s	2$
	move.l	(a4)+,d0
	add.l	stackbot(a6),d0
	move.l	d0,currframe(a6)
	move.w	(a4)+,block_index(a6)
	move.w	(a4)+,block_number(a6)
	movea.l blockp(a6),a0
	move.w	16(a0),-(a7)
	moveq.l #1,d0
	move.w	statmem(a6),d1
	bsr	LBL81
	bne.s	3$
	movea.l blockp(a6),a0
	move.w	(a7)+,16(a0)
	bsr	LBL75
	bsr	swapblock
	bsr	LBL76
	moveq.l #1,d0
	bsr	storyopcl
	bra	jumpt
1$	bsr	LBL78
	moveq.l #1,d0
	bsr	storyopcl
	bra	jumpf
2$	move.w	d0,-(a7)
	bsr	LBL75
	move.w	(a7)+,d0
	lea.l	badsave,a0
	bra	internal_error
3$	move.w	d0,-(a7)
	bsr	LBL75
	move.w	(a7)+,d0
	lea.l	savfrerr,a0
	bra	internal_error

*--- bytecode RESTARTop ---*
* parameters: none
* returns: nothing
* side effects: RESTARTops the story

RESTARTop
	bsr	newline		; start a new line
	clr.w	lines_printed(a6) ; clear lines_printed
	jsr	clear_sc	; clear the screen
	bra	rstart		; RESTARTop

*--- bytecode QUITop ---*

QUITop
	bra	shutdown	; shut down the game

*--- bytecode VERIFYop---*
* parameters: none
* returns: nothing
* side effects: computes file checksum to see if it is okay
*		operates like conditional jump on next address

VERIFYop
	lea.l	banner,a0		; print interpreter version
	bsr	printcrstring
	movea.l blockp(a6),a0		; get a certain string address
	move.w	26(a0),d0		; get the offset
	bsr	padr2adr		; compute block number and id
	move.w	d0,d4			; save block number
	move.w	d1,d5			; save block index
	clr.w	d0			; clear d0
	moveq.l #64,d1			; d1 = 64 don't checksum the header
	clr.w	d3			; d3 = 0
	move.w	permblocks(a6),-(a7)	; make believe that we have
	clr.w	permblocks(a6)		; no permanently allocated blocks
1$	bsr	fetchbyte		; get a byte (in d2)
	add.w	d2,d3			; add it to d3
	cmp.w	d0,d4			; have we reached our block number yet?
	bne.s	1$			; no.
	cmp.w	d1,d5			; or our block id?
	bne.s	1$			; no
	move.w	(a7)+,permblocks(a6)	; restore status quo
	movea.l blockp(a6),a0
	move.w	28(a0),d0		; get checksum
	cmp.w	d0,d3			; compare them
	bne.s	2$			; are they equal?
	bra	jumpt			; yes
2$	bra	jumpf			; no

*--- bytecode_machine ---*
* parameters: none
* returns: doesn't
* side effects: decodes bytecode instructions in the story file and calls
*		the appropriate routines with the appropriate parameters.

bytecode_machine
	bsr	fetch_bytecode		; fetch an instruction

	bsr	dbugbc			; copy bytecode to debug outlet

	cmpi.b	#128,d0
	bcs.s	2$			; carry is set when d0 < 128
	cmpi.b	#176,d0
	bcs.s	1$			; carry is set when d0 < 176
	cmpi.b	#192,d0
	bcc	6$			; carry is clear when d0 >= 192
* bytecodes 176...191  no parameters *
	andi.w	#15,d0			; low nibble encodes the routine
	add.w	d0,d0			; times 2 for word offset table
	lea.l	funtab1,a1		; table with function offsets
	move.w	0(a1,d0.w),d2		; get offset
	lea.l	bytecode_base,a1	; base address
	jsr	0(a1,d2.w)		; execute the routine
	bra.s	bytecode_machine	; next instruction
* bytecodes 128...175  one parameter *
1$	move.w	d0,d2			; save bytecode
	andi.w	#15,d2			; low nibble encodes routine
	add.w	d2,d2			; times 2 for word offset table
	lsr.w	#4,d0			; bits 4 and 5 are the parameter
	andi.w	#3,d0			; for the parameter fetcher
	bsr	fetch_parameters	; fetch parameters
	lea.l	funtab2,a1		; table fith function offsets
	move.w	0(a1,d2.w),d2		; get offset
	lea.l	bytecode_base,a1	; base address
	jsr	0(a1,d2.w)		; execute the routine
	bra.s	bytecode_machine	; next instruction
* bytecodes 0...127  2 parameters *
2$	move.w	d0,d2			; save bytecode
	moveq.l #1,d0			; bit 6 encodes parameter for parameter
	btst.l	#6,d2			; fetcher for the first parameter:
	beq.s	3$			; off means 1
	moveq.l #2,d0			; on means 2
3$	bsr	fetch_parameters	; fetch first parameter
	move.w	d0,d1			; save firt parameter
	moveq.l #1,d0			; bit 5 encodes parameter for parameter
	btst.l	#5,d2			; fetcher for the second parameter
	beq.s	4$			; off means 1
	moveq.l #2,d0			; on means 2
4$	bsr	fetch_parameters	; fetch second parameter
	exg.l	d0,d1			; restore order in parameters
	andi.w	#31,d2			; low 5 bits encode routine
	add.w	d2,d2			; times 2 for word offset table
	lea.l	funtab3,a1		; table with function offsets
	move.w	0(a1,d2.w),d2		; get offset
	lea.l	bytecode_base,a1	; base address
	lea.l	0(a1,d2.w),a1		; get routine address
	cmpi.w	#$4E71,(a1)		; routine wants params in registers?
	bne.s	5$			; yes
	lea.l	param3(a6),a0		; no. put parameters into array
	move.w	d1,-(a0)		; second
	move.w	d0,-(a0)		; first
	move.w	#2,-(a0)		; number of parameters
5$	jsr	(a1)			; call routine
	bra	bytecode_machine	; next instruction
* bytecodes 192...255  up to 4 parameters *
6$	move.w	d0,d2			; save bytecode
	moveq.l #4,d3			; number of parameter mods on stack
	bsr	fetch_bytecode		; get parameter mod byte
	move.w	d0,-(a7)		; 4 packets of 2 bits each encode the
	lsr.w	#2,d0			; parameters passed to
	move.w	d0,-(a7)		; fetch_parameters.
	lsr.w	#2,d0			; they are read from left to right
	move.w	d0,-(a7)
	lsr.w	#2,d0
	move.w	d0,-(a7)
	clr.w	d4			; number of parameters found
	lea.l	param1(a6),a1		; parameter array base
7$	move.w	(a7)+,d0		; get first parameter modifier
	andi.w	#3,d0			; 2 bits only
	cmpi.w	#3,d0			; 3 means no more parameters
	beq.s	8$			; so stop looking for more
	addq.w	#1,d4			; found a parameter
	bsr	fetch_parameters	; get it
	move.w	d0,(a1)+		; put it in array
	subq.w	#1,d3			; one less on stack
	bne.s	7$			; got any more?
	bra.s	9$			; no
8$	subq.w	#1,d3			; correct stack
	add.w	d3,d3			; times 2: words on stack
	add	d3,a7			; retouch stack pointer
9$	lea.l	nparams(a6),a0		; parameter base
	move.w	d4,(a0)			; number of parameters
	andi.w	#63,d2			; low six bits encode routine
	add.w	d2,d2			; times 2 for word offset table
	lea.l	funtab3,a1		; table fir function offsets
	move.w	0(a1,d2.w),d2		; get offset
	lea.l	bytecode_base,a1	; get base address
	lea.l	0(a1,d2.w),a1		; get function address
	cmpi.w	#$4E71,(a1)		; does it want params in array?
	beq.s	10$			; yes
	addq.l	#2,a0			; not, put them in registers
	move.w	(a0)+,d0		; first parameter
	move.w	(a0)+,d1		; second
	move.w	(a0)+,d2		; third
	move.w	(a0)+,d3		; fourth
10$	jsr	(a1)			; call routine
	bra	bytecode_machine	; next instruction

*--- bytecode ILLEGALop ---*
* This is the illegal operation; it shuts down the system.

ILLEGALop
	clr.w	d0
	lea.l	badop,a0
	bra	internal_error

*--- prndecrypt ---*
* parameters: d0: segment number
*	      d1: segment index
* returns: d0, d1: end of string
* side effects:

prndecrypt
	movem.l d2-d5,-(a7)	; save registers
	clr.w	d4		; clear this
	clr.w	d5		; clear that
1$	bsr	fetchint	; fetch integer referenced by d0,d1 into d2
	movem.w d0-d2,-(a7)	; save reference & int
	moveq.l #2,d3		; prepare for little loop (2 passes)
2$	move.w	d2,-(a7)	; save int
	asr.w	#5,d2		; divide int by 32
	dbf	d3,2$(pc)	; loop: next
	moveq.l #2,d3		; load d3 with a 2
16$	move.w	(a7)+,d2	; restore int
	andi.w	#31,d2		; mask out high bits from char
	tst.w	d4		; test d4 (0 on first pass)
	bpl.s	3$		; >0?
	asl.w	#1,d2		; no. d2 *= 2
	movea.l abbrevp(a6),a0	; l11: pointer into the story
	add.w	w9(a6),d2	; w9: an offset
	add	d2,a0		; a0 now points at something in particular
	bsr	getint		; read 2 bytes from a0 & make an int
	bsr	padr2adr	; put high byte into d0, low byte*2 into d1
	bsr.s	prndecrypt	; print this one
	bra	14$		; d4 = d5; contiue
3$	cmpi.w	#3,d4		; test d4
	blt.s	5$		; less than 3?
	bgt.s	4$		; greater than 3?
	bset.l	#14,d4		; equals three. set bit 15
	move.b	d2,d4		; put char into low byte.
	bra.s	15$		; continue
4$	andi.w	#3,d4		; d4 was > 3. mask out bits.
	asl.w	#5,d4		; times 32
	or.w	d2,d4		; or it with char
	move.w	d4,d0		; char to print
	bra.s	13$		; outputchar; d4 = d5; continue
5$	cmpi.w	#6,d2		; d4 was < 3. test d2
	blt.s	8$		; d2 < 6?
	cmpi.w	#2,d4		; test d4
	bne.s	7$		; d4 != 2
	cmpi.w	#7,d2		; test d2
	beq.s	6$		; d2 == 7?
	bgt.s	7$		; d2 > 7?
	addq.w	#1,d4		; d4++
	bra.s	15$		; continue
6$	bsr	newline		; d2 == 7. print a newline
	bra.s	14$		; d4 = d5; continue
7$	move.w	d4,d1		; d4 != 2 and d2 > 7. d1 = d4
	mulu.w	#26,d1		; d1 *= 26
	add.w	d2,d1		; d1 += d2
	lea.l	alphab,a0	; get the alphabet
	move.b	-6(a0,d1.w),d0	; d0 = alphabet[d1 - 6]
	bra.s	13$		; outputchar(d0); d4 = d5; continue;
8$	tst.w	d2		; d2 < d6. test d2
	bne.s	9$		; d2 != 0?
	moveq.l #32,d0		; d0 = 32
	bra.s	13$		; outputchar(d0); d4 = d5; continue;
9$	cmpi.w	#3,d2		; test d2
	bgt.s	10$		; d2 > 3?
	bset.l	#15,d4		; set bit 15
	subq.w	#1,d2		; d2--
	asl.w	#6,d2		; d2 *= 64
	move.w	d2,w9(a6)	; store it
	bra.s	15$		; continue
10$	subq.w	#3,d2		; d2 > 3. d2 -= 3
	tst.w	d4		; test d4
	bne.s	11$		; d4 != 0?
	move.w	d2,d4		; d4 = d2
	bra.s	15$		; continue
11$	cmp.w	d2,d4		; compare d2,d4
	beq.s	12$		; ==?
	clr.w	d4		; no. d4 = 0
12$	move.w	d4,d5		; d5 = d4
	bra.s	15$		; continue
13$	bsr	outputchar	; "output d0"
14$	move.w	d5,d4		; "d4 = d5"
15$	dbf	d3,16$(pc)	; "continue"
	movem.w (a7)+,d0-d2	; restore segment indices and char
	tst.w	d2		; test the char
	bpl	1$		; char > 0?
	movem.l (a7)+,d2-d5	; restore used registers
	rts			; return.

;;; zshift - find the z-shift for an ASCII char
;;; param - d0 - an ascii character
;;; return - d0 - 0 for lower case, 1 for upper case, 2 for other 

zshift
	tst.b	d0		; ascii code to be converted
	bne.s	1$		; not 0
	moveq.l #3,d0		; ascii(0) is mapped to a z(4)
	bra.s	4$		; and return
1$	cmpi.b	#$61,d0		; $61 = 'a'
	blt.s	2$		; not lower case letter
	cmpi.b	#$7A,d0		; $7A = 'z'
	bgt.s	2$		; not lower case letter
	clr.w	d0		; lower case letters classify as 0
	bra.s	4$		; return
2$	cmpi.b	#$41,d0		; $41 = 'A'
	blt.s	3$		; not upper case letter
	cmpi.b	#$5A,d0		; $5A = 'Z'
	bgt.s	3$		; not upper case letter
	moveq.l #1,d0		; upper case letters get a 1
	bra.s	4$		; return
3$	moveq.l #2,d0		; other letters get a 2
4$	rts

;;; zcode - find the z-code of an a ASCII symbol, without shift
;;; param - d0 - the ascii char to convert
;;; return - d0 - the z-code of the ascii char

zcode
	lea.l	alphab,a0	; fetch the alphabet (0-terminated)
1$	cmp.b	(a0)+,d0	; find the character in the alphabet
	beq.s	2$		; found?
	tst.b	(a0)		; end of alphabet?
	bne.s	1$		; next char in alphabet
	clr.w	d0		; not found - return a 0
	bra.s	4$		; return
;;; at this point we have found the character in the alphabet
2$	move.l	a0,d0		; compute the offset of the char in alphabet
	lea.l	alphab,a0	; base
	sub.l	a0,d0		; -> offset in d0
	addq.w	#5,d0		; +5 (shift codes)
3$	cmpi.w	#32,d0		; upper case character?
	blt.s	4$		; no, return
	subi.w	#26,d0		; shift down (uc->lc, sym->uc->lc)
	bra.s	3$		; second loop only for sym & num
4$	rts

;;; word2dict - convert the 6-letter word into dictionary format
;;; parameters:	a0 - buffer for the dictionary-format word
;;;             a1 - buffer containing at most the first 6 characters of a word
word2dict
	movem.l d1-d2/a2,-(a7)	; save these
	movea.l a0,a2		; addr of dict buffer
	move.w	#6,d2		; loop counter (at most 6 chars)
6$	clr.w	d1		; use a clean word
	move.b	(a1)+,d1	; get byte from buffer
	beq.s	3$		; zero? goto 3
	move.w	d1,d0		; param to zshift
	bsr.s	zshift		; compute shift
	tst.w	d0		; is d1 lower case?
	beq.s	1$		; yes
	addq.w	#3,d0		; upper case shift - 4, other - 5
	move.w	d0,-(a7)	; push the shift code
	subq.w	#1,d2		; decrement counter
	beq.s	4$		; completely converted?
1$	move.w	d1,d0		; param to zcode
	bsr.s	zcode		; compute zcode
	tst.w	d0		; does it have a z-code?
	bne.s	2$		; yes
	moveq.l #6,d0		; else, get the ascii extender
	move.w	d0,-(a7)	; and push it
	subq.w	#1,d2		; decrement counter
	beq.s	4$		; and if the stack is full, break
	move.w	d1,d0		; get the ascii code
	asr.w	#5,d0		; get the high bits
	move.w	d0,-(a7)	; push them
	subq.w	#1,d2		; decrement counter
	beq.s	4$		; and break if full
	move.w	d1,d0		; get ascii code
	andi.w	#$1F,d0		; mask out the low bits
;;; here, we have finally found a z-code for a character
2$	move.w	d0,-(a7)	; and push it
	subq.w	#1,d2		; and decrement the counter
	bne.s	6$		; still some chars left?
	bra.s	4$		; no, completely converted

3$	move.w	#5,-(a7)	; fill the rest of the dict word with 5's
	subq.w	#1,d2		; decrement counter
	bne.s	3$		; 
;;; at this point, we have 6 z-characters (16bit) on the stack in reverse order
4$	move.w	#12,d2		; 6 words on stack
	movea.l a7,a0		; bottom of stack (last word)
	add	d2,a0		; beg of z-char list
	move.w	#2,d1		; counter for loop (build 2 words)
	
5$	move.w	-(a0),d0	; get first char in word
	asl.w	#5,d0		; move it left
	or.w	-(a0),d0	; get second char
	asl.w	#5,d0		; move both left
	or.w	-(a0),d0	; get third char
	move.w	d0,(a2)+	; stuff word with 3 zchars into buffer
	subq.w	#1,d1		; decrement counter
	bne.s	5$		; next loop?
	
	bset.b	#7,-2(a2)	; set `end of string' bit
	add	d2,a7		; throw away the 6 z-codes
	movem.l (a7)+,d1-d2/a2	; bye
	rts			; return

*--- makeopstruct ---*
* parameters  d1  buffer size
*	      d2  number of columns on device
*	      a1  address of output fun
*	      a2  address of another fun (formatter?)
* returns  a0  address of struct
* side effects	allocates memory for output device struct and builds it.

makeopstruct
	move.w	d1,d0		; buffer size for a line?
	add.w	#28,d0		; plus 28 bytes for struct
	ext.l	d0		; make it loooong
	bsr	Malloc		; allocate buffer space
	move.l	a0,d0		; address of buffer
	add.l	#28,d0		; offset 28 bytes  buffer start
	move.l	d0,0(a0)	; address of string buffer
	move.l	d0,6(a0)	; copy of string buffer
	move.w	d2,4(a0)	; number of columns on output device
	clr.w	10(a0)		;
	move.l	a1,16(a0)	; output fun
	move.l	a2,12(a0)	; other fun
	clr.b	24(a0)		; first char in buffer
	rts

;;; buffer_output -
;;; parameters   d0  a char
;;;              a0  a buffer struct
;;; returns      nothing
;;; side effects uses the buffer struct to output the character
;;;              to the appropriate device.

buffer_output
	movem.l d4/a1-a4,-(a7)	; save them all
	move.w	d0,d4		; char
	movea.l a0,a4		; buffer struct
	move.w	10(a4),d0	; buffer.n
	cmp.w	4(a4),d0	; buffer.w
	blt.s	8$		; if buffer.n < buffer.w go to 8$
	cmpi.b	#32,d4		; if c != <space> go to 1$
	bne.s	1$
	move.l	6(a4),d0	; buffer.last
	movea.l (a4),a0		; buffer.first
	movea.l 16(a4),a3	; buffer.output
	jsr	(a3)
	clr.w	10(a4)		; buffer.n = 0
	move.l	(a4),6(a4)	; buffer.last = buffer.first
	clr.b	24(a4)		; buffer.bytes[0] = 0
	bra.s	9$		; return
;;; --- flag 1 >= flag 2 AND char != <space> ---*
;;; this is a loop which finds the last space; exits to 4$ if
;;; there was one, and to 3$ if not.
1$	movea.l (a4),a1		; a1 = buffer.first
	movea.l 6(a4),a2        ; a2 = buffer.last
2$	cmpi.b	#32,-(a2)	; if last char of string = <space>
	beq.s	4$		; then go to 4$ (found a space)
	cmpa.l	a1,a2		; while buffer.last > buffer.first
	bgt.s	2$		; then go to 2$ (check previous byte)
;;; this is when no space was found
3$	tst.b	24(a4)		; if buffer.bytes[0] != 0 then go to 5$
	bne.s	5$              ; with the a2 unchanges
	movea.l 6(a4),a2	; else load the real buffer.last into a2
	bra.s	5$		; and go to 5$
* this is when a2 points at <space>. a2 contains the new buffer.last
4$	movea.l a2,a0           ; make a copy
	addq.l	#1,a2           ; advance
	cmpa.l	a1,a0		; if first byte is space
	beq.s	3$              ; which is equivalent to no space found
;;; ANYHOW, we end up here. a1 still is buffer.first,
;;; and a2 is the new buffer.last
5$	move.l	a2,d0		; end of string, for better or worse
	movea.l a1,a0		; beginning of string
	movea.l 16(a4),a3	; buffer.output
	jsr	(a3)
	clr.w	10(a4)		; buffer.N = 0
	clr.b	24(a4)		; buffer.bytes[0] = 0
	bra.s	7$		; go to 7$

6$	move.b	(a2)+,d0	; pull char from beyond end
	move.b	d0,(a1)+	; into beginning
	movea.l 12(a4),a0       ; buffer.info
	jsr	(a0)
	add.w	d0,10(a4)	; buffer.n += return value
7$	cmpa.l	6(a4),a2	; if real end of string < new end of string
	blt.s	6$
	move.l	a1,6(a4)	; new buffer.last

8$	movea.l 6(a4),a0	; append char argument
	move.b	d4,(a0)+	; to end of string
	move.l	a0,6(a4)
	move.w	d4,d0		; char argument
	movea.l 12(a4),a0	; buffer.info
	jsr	(a0)
	add.w	d0,10(a4)	; buffer.n += return value
9$	movem.l (a7)+,d4/a1-a4
	rts

*--- newline and bytecode outputchar ---*
* parameters  d0  (for outputchar) a character (newline puts \cr there)
* returns
* side effects	sets flags for (d0 == 0)

newline
	moveq.l #13,d0
outputchar
	nop			; arg pass flag for bytecode decoder
	tst.w	mystrflag(a6)	; if mystrflag(a6) is not zero,
				; goto char2mystring
	bne	char2mystring
	cmpi.b	#9,d0		; change TAB into <space>
	bne.s	1$
	moveq.l #32,d0
1$	move.w	d0,-(a7)	; save d0 onto stack for final test
	tst.w	w1(a6)		; if w1(a6) not equal to 0, call printif
	beq.s	2$
	move.w	(a7),d0		; restore d0
	bsr.s	printif		; call printif
2$	nop
	bsr	tstbit		; call tstbit - script bit on ?
	beq.s	3$		; no -> return
	tst.w	sel_wind(a6)	; upper window selected?
	bne.s	3$		; yes -> return
	move.w	(a7),d0		; stuff it into x-script
	bsr.s	lprintif
3$	nop
	nop
	tst.w	(a7)+		; was original d0 zero?
	rts

*--- printif ---*
* parameters: d0 char to print
* returns:
* side effects:

printif
	tst.w	conbufflag(a6)	; if conbufflag(a6) equals 0, go to printchar
	beq	printchar
	cmpi.b	#13,d0		; if d0 is not \cr, go to buffer_output with
	beq.s	1$		; constring(a6) in a0.
	movea.l constring(a6),a0
	bra	buffer_output
1$	move.l	a4,-(a7)
	movea.l constring(a6),a4
	movea.l (a4),a0
	move.l	6(a4),d0
	bsr	cprint
	move.l	(a4),6(a4)
	clr.w	10(a4)
	clr.b	24(a4)
	movea.l (a7)+,a4
	rts

;;; flush_con_buf - flush the console buffer. this is used to print the
;;; prompt before input is accepted. buffer.n is set to 0, which is
;;; okay here, because the rest of the line won't be under buffer control.
flush_con_buf
	movea.l constring(a6),a0	; get the costring struct
	clr.w	10(a0)			; buffer.n = 0

;;; printcon - this really only flushed the buffer
printcon
	move.l	a4,-(a7)		; save bytecode stack pointer
	movea.l constring(a6),a4	; put constring in a4
	movea.l 0(a4),a0		; beginning of string
	move.l	6(a4),d0		; end of string
	bsr	preprint		; print the string
	move.l	0(a4),6(a4)		; clear string
	move.b	#1,24(a4)		; buffer.bytes[0] = 1
	movea.l (a7)+,a4		; restore bytecode stack pointer
	rts

*--- get_char_echo1 ---*

get_char_echo1
	bra	get_char_echo

*--- lprintif ---*
* parameters  d0  a char
* returns  nothing
* side effects

lprintif
	cmpi.b	#13,d0		; if d0 != 13
	beq.s	1$
	movea.l lpstring(a6),a0
	bra	buffer_output
1$	move.l	a4,-(a7)	; else
	movea.l lpstring(a6),a4
	movea.l (a4),a0		; addr of string
	move.l	6(a4),d0	; end of string
	bsr	lprint
	move.l	(a4),6(a4)	; move beginning of string to end of string
	clr.w	10(a4)		; clear it
	movea.l (a7)+,a4
	rts

;;; lprint_input - echo the string between a0 and d0 to the printer,
;;; if scripting is on.
lprint_input
	movem.l d1/a1,-(a7)
	movea.l a0,a1
	move.l	d0,d1
	bsr	tstbit
	beq.s	3$
	sub.l	a1,d1
	beq.s	2$
1$	move.b	(a1)+,d0
	bsr.s	lprintif
	subq.w	#1,d1
	bgt.s	1$
2$	moveq.l #13,d0
	bsr.s	lprintif
3$	movem.l (a7)+,d1/a1
	rts

*--- swapblock ---*
* parameters  none
* returns  nothing
* side effects	make sure block containing the byte referenced by
*		block_number(a6) and block_index(a6) is in memory, and load
*		it into floating block memory if necessary.

swapblock
	move.l	d1,-(a7)	; save d1
* compute file offset of byte we want to access *
	move.w	block_number(a6),d0	; block number
	ext.l	d0		; make it loooong
	asl.l	#8,d0
	asl.l	#1,d0		; times 512
	move.w	block_index(a6),d1	; block_index(a6)
	ext.l	d1		; again, make it long
	add.l	d1,d0		; plus the above = byte offset,
	move.w	d0,d1		; and put it into d1, too.
* compute block number and index of byte we want to access *
	andi.w	#511,d1		; mask out high bits (remainder of next div)
	move.w	d1,block_index(a6)	; put it into block_index(a6)
	asr.l	#8,d0
	asr.l	#1,d0		; divide by 512
	move.w	d0,block_number(a6)	; put quotient into block_number(a6)
* is it still the current block? *
	cmp.w	currblock(a6),d0	; compare
	beq.s	3$		; same-same? okay, return
	move.w	d0,currblock(a6)	; no, update currblock(a6)
	tst.l	l23(a6)		; is this zero?
	beq.s	1$		; yes, skip this
	movea.l l23(a6),a0		; no, put fltblkid(a6)...
	move.l	fltblkid(a6),2(a0)	; ...into 2(l23(a6))
* is it a permanently loaded block? *
1$	cmp.w	permblocks(a6),d0	; compare
	blt.s	2$		; yes, it is a permanent block.
	bsr.s	swapfltblk	; no. go swapfltblk (which reads a block)
	move.l	a0,currblock_p(a6)	; put this here
	movea.l currfltrec(a6),a0	; and put this...
	move.l	a0,l23(a6)		; ...here
	move.l	#-1,2(a0)	; and -1 here.
	bra.s	3$		; skip this, and ultimately, return
* if it was a permanent block *
2$	bsr	utimes512	; *512
	add.l	blockp(a6),d0	; +blockp(a6)
	move.l	d0,currblock_p(a6)	; update currblock_p(a6)
	clr.l	l23(a6)		; and clear l23(a6)
* return, restoring d1. *
3$	move.l	(a7)+,d1	; anyway, restore d1
	rts			; and GO BACK.

*--- swapfltblk ---*
* parameters  d0  block number (of a floating block) to read
* returns  a0  currfltblk(a6)_p(a6) (address of currently swapped
* in floating block)
* side effects

swapfltblk
	cmp.w	currfltblk(a6),d0	; currfltblk(a6) == parameter?
	beq.s	4$		; yep, done.
	move.w	d0,currfltblk(a6)	; no, update currfltblk(a6)
	addq.l	#1,fltblkid(a6) ; increase fltblkid(a6)
	movea.l fltblktab(a6),a0	; get me fltblktab(a6)
* determine entry in floating block table *
1$	cmp.w	(a0),d0		; is this the block in question?
	bne.s	3$		; no. go to 3$
	cmp.w	currblock(a6),d0 ; yes. is the block in question the active one?
	beq.s	2$		; yes. skip this.
	move.l	fltblkid(a6),2(a0)	; else put this here.
* compute address of this buffer *
2$	move.l	a0,currfltrec(a6)	; current table record
	move.l	a0,d0		; ...and here
	sub.l	fltblktab(a6),d0	; the how manieth entry (*8)
	asl.l	#6,d0		; byte offset from start of floating blocks
	add.l	fltblockp(a6),d0	; address of this buffer
	move.l	d0,currfltblk_p(a6)	; goes here.
	bra.s	4$		; and we're done.
* maybe we should load the block from disk after all, before looking too far *
3$	addq.l	#8,a0		; next record
	cmpi.w	#$FFFF,(a0)	; block free?
	bne.s	1$		; no, get next one.
	move.l	a1,-(a7)	; yes, so save a1
	bsr.s	findfltbuf	; and go to findfltbuf
	move.l	a0,currfltblk_p(a6)	; return value
	move.l	a1,currfltrec(a6)	; current table record
	move.w	currfltblk(a6),d0	; block number
	move.w	d0,(a1)+	; which goes here,
	move.l	fltblkid(a6),(a1)	; pointer to block buffer
	bsr	readblock	; and now we can read this block
	movea.l (a7)+,a1	; and restore a1
4$	movea.l currfltblk_p(a6),a0	; and return currfltblk_p(a6)
	rts			; there you go.

*--- findfltbuf ---*
* parameters  a1
* returns  a0  address of a free block buffer
*	   a1  address of this floating block table index
* side effects	looks for buffer to use.

findfltbuf
	movea.l fltblktab(a6),a0	; get me fltblktab(a6)
	addq.l	#2,a0		; plus 2
	move.l	#$FF,d0		; and load d0 with 255
1$	cmp.l	(a0),d0		; is bufid <= 255
	bls.s	2$		; yes? go there.
	move.l	(a0),d0		; no. update d0
	movea.l a0,a1		; and put ptr to bufid here
2$	addq.l	#6,a0		; a0 plus 6  next record
	cmpi.w	#-1,(a0)+	; is it empty?
	bne.s	1$		; no. go up
	subq.l	#2,a1		; yes, make a1 point to record
	move.l	a1,d0		; a1
	sub.l	fltblktab(a6),d0	; -fltblktab(a6)
	asl.l	#6,d0		; *64
	add.l	fltblockp(a6),d0	; +fltblockp(a6)
	movea.l d0,a0		; =a0
	rts

*--- initstoryfile ---*
* parameters  none
* returns  nothing
* side effects	loads address of my_timer into new_time, opens story
*		file, outputs error message if something went wrong.

initstoryfile
	lea.l	my_timer,a0
	move.l	a0,new_time
	bsr	openstory	; returns 0 on success
	bne.s	1$
	rts			; everything okay.
1$	lea.l	opners,a0	; error message
	bra	internal_error

*--- Malloc ---*
* parameters  d0  long, number of bytes to allocate
* returns  a0  address of storage
* side effects	allocates core memory.

Malloc
	movem.l d1-d7/a1-a5,-(a7)
	move.l	d0,-(a7)
	move.w	#$48,-(a7)		; GEMDOS $48  Malloc
	trap	#1
	add	#6,a7
	tst.l	d0
	beq.s	mem_error
	movea.l d0,a0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- mem_error ---*

mem_error
	clr.w	d0
	lea.l	nomem,a0
	bra	internal_error

*--- qfreemem ---*

qfreemem
	movem.l d1-d7/a1-a5,-(a7)
	move.l	#-1,-(a7)
	move.w	#$48,-(a7)		; GEMDOS $48  Malloc
	trap	#1
	add	#6,a7
	sub.l	#10240,d0		; 10 k for fsel
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- Random ---*
* parameters  none
* returns  a random 24-bit number
* side effects	none

Random
	move.w	#$11,-(a7)		; XBIOS 17  Random
	trap	#14
	addq.l	#2,a7
	rts

*--- unused code ---*
*
* 0B0C1C	  CLR.L	    D0
* 0B0C1E	  RTS

*--- otherfun ---*

otherfun
	moveq.l #1,d0
	rts

*--- shutdown ---*

shutdown
	bsr	closestory
	tst.w	pending_input(a6)		; input pending?
	bne.s	1$
	suba.l	a0,a0
	bsr	printcrstring
	lea.l	hitkys,a0	; "Strike any key to exit "
	bsr	printstdstring
	bsr.s	fetchchar
1$	unlk	a6
	rts

*--- unused code ---*
*
* 0B0C44	  RTS

*--- printtitle	 c bind for print_ti ---*
* This in the bind for print_ti(). Parameters are
*	a0  room name string
*	d0  length of room name
*	a1  score/time info string
*	d1  legth of score/time string

printtitle
	movem.l d2-d7/a2-a5,-(a7)
	move.w	d1,-(a7)
	move.l	a1,-(a7)
	move.w	d0,-(a7)
	move.l	a0,-(a7)
	jsr	print_ti
	add	#12,a7			; bug?? a7??
	movem.l (a7)+,d2-d7/a2-a5
	rts

*--- get_char_echo ---*

get_char_echo
	bsr.s	fetchchar	; get a character
	cmpi.b	#8,d0		; is is backspace?
	bne.s	1$		; no, skip to 1$
	tst.w	inbufcnt(a6)	; any characters input so far?
	beq.s	2$		; no skip to end
	subq.w	#2,inbufcnt(a6)	; yes, go back 2 (because we advance 1 next)
1$	addq.w	#1,inbufcnt(a6)	; advance input count
	move.w	d0,-(a7)	; push input on stack
	bsr.s	showchar	; call showchar
	move.w	(a7)+,d0	; pop input
2$	rts			; return

*--- fetchchar -- C bind ---*
* This routine fetches a character from the keyboard and filters out
* everything but the printable ascii codes, <cr>, and <bs>. Also, w1(a6) and
* w2 are each loaded with a 1. The char is not echoed by the C fun.

fetchchar
	move.w	#1,pending_input(a6)	; signal `input pending'
	move.w	#1,lines_printed(a6)
	movem.l d1-d7/a1-a5,-(a7)
1$	jsr	get_a_ch			C fun
	andi.w	#$FF,d0
	cmpi.b	#13,d0
	beq.s	2$			<cr>? -> end
	cmpi.b	#8,d0
	beq.s	2$			<bs>? -> end
	cmpi.b	#32,d0
	blt.s	1$			other control char? ignore, get next
	cmpi.b	#127,d0
	bgt.s	1$			greater than ascii?   ---- " ----
	blt.s	2$			printable ascii? okay
	moveq.l #8,d0			127 is same as <bs>
2$	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- printchar and showchar -- C bind ---*
* This is the bind for the C function print_a_(). The routine takes a
* parameter in d0, which is the ascii value to print, and masks out the
* high byte. Also it loads pending_input(a6) with a zero.

printchar
	clr.w	pending_input(a6)	; no more pending input
showchar
	movem.l d1-d7/a1-a5,-(a7)	; save registers
	andi.w	#$FF,d0			; mask out high byte
	move.b	d0,-(a7)
	jsr	print_a_		; print_a_()
	addq.w	#2,a7
	movem.l (a7)+,d1-d7/a1-a5	; restore registers
	rts

*--- prmore ---*

prmore
	move.w	nlines,d0
	sub.w	text_top,d0
	addq.w	#1,lines_printed(a6)
	cmp.w	lines_printed(a6),d0
	bne.s	1$
	movem.l d1-d7/a1-a5,-(a7)
	bsr	SHOW_STATUSop
	movem.l (a7)+,d1-d7/a1-a5
	lea.l	mores,a0		"[MORE]"
	moveq.l #6,d0
	bsr.s	printstring
	bsr	fetchchar
	subq.w	#6,crsr_col
	lea.l	spaces,A0		"      "
	moveq.l #6,d0
	bsr.s	printstring
	subq.w	#6,crsr_col
	move.w	#2,lines_printed(a6)
1$	rts

*--- printstring -- C bind ---*
* This function calls the C function print(), which takes a string and
* its length as parameters. pending_input(a6) is loaded with zero.


printstring
	clr.w	pending_input(a6)		; no more pending input
	movem.l d1-d7/a1-a5,-(a7)
	move.w	d0,-(a7)
	move.l	a0,-(a7)
	jsr	print
	addq.w	#6,a7
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- cprint ---*

cprint	tst.w	sel_wind(a6)
	bne.s	1$
	movem.l d0/a0,-(a7)
	bsr.s	prmore
	movem.l (a7)+,d0/a0
1$	sub.l	a0,d0
	ble.s	2$
	bsr.s	printstring
2$	moveq.l #13,d0
	bsr	printchar
	rts

*--- preprint ---*
* parameters: a0: pointer at string
*	      d0: pointer at end of string
* returns: nothing
* side effects: prints the string

preprint
	sub.l	a0,d0
	ble.s	1$
	bsr.s	printstring
1$	rts

*--- printstdstring ---*
* parameters: a0: a null-teminated string
* returns:
* side effects:

printstdstring
	movem.l a1-a2,-(a7)	; save registers
	movea.l a0,a1		; move string here
1$	clr.w	d0		; do: char to output
	move.b	(a1)+,d0	; get char from string
	beq.s	2$		; done?
	bsr	outputchar	; no. output char
	bra.s	1$		; next char
2$	tst.w	conbufflag(a6)	; conbufflag cleared?
	beq.s	3$		; yes. don't flush buffer
	bsr	printcon	; flush buffer
3$	movem.l (a7)+,a1-a2
	rts

*--- printcrstring ---*
* parameters  a0  a zero-terminated string
* returns  doesn't, always goes to newline
* side effects	prints the string and a \cr

printcrstring
	move.l	a0,d0		; is it a non-zero string?
	beq.s	1$
	bsr.s	printstdstring	; yes
1$	bra	newline		; print \cr

*--- internal_error ---*
* parameters  a0  error message string (zero terminated)
*	      d0  negative error number (typically GEMDOS error)
* returns  doesn't; always goes to shutdown
* side effects	prints "internal error", a GEMDOS error number, and
*		a message string, then terminates the program.

internal_error
	move.w	d0,d1
	movea.l a0,a1
	bsr	newline		; print a \cr
	lea.l	inters,a0
	bsr.s	printstdstring	; print "internal error"
	tst.w	d1		; test error number
	beq.s	1$		; for non-GEMDOS error numbers
	move.b	#35,d0		; ascii for '#'
	bsr	outputchar	; print a #
	move.w	d1,d0		; get error number
	bsr	PRINT_NUMop  ; print error number
1$	bsr	newline		; print a \cr
	move.l	a1,d0		; did we have a string passed?
	beq.s	2$		; no, go to end
	movea.l a1,a0		; yes
	bsr.s	printcrstring	; print the string, \cr terminated.
2$	bra	shutdown

*--- tstbit --*

tstbit
	tst.l	blockp(a6)	; these are my bug fixes
	beq.s	1$		; (if blockp is not initialized)
	movea.l blockp(a6),a0
	btst.b	#0,17(a0)
1$	rts

*--- cprnos ---*
* parameters  none
* returns  nothing
* side effects	sets flags; eq means printer ready

cprnos	movem.l d1-d7/a1-a5,-(a7)
	move.w	#$11,-(a7)		; GEMDOS $11  Cprnos
	trap	#$1
	addq.l	#2,a7
	tst.l	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- cprnout -- bind for Cprnout ---*
* parameters  d0  character to print on printer
* returns  nothing
* side effects	prints the passed character on the printer if it is ready,
*		else it waits.

cprnout
	movem.l d1/a1,-(a7)
	move.w	d0,-(a7)
	move.w	#5,-(a7)	; GEMDOS 5  Cprnout
1$	bsr.s	cprnos
	beq.s	1$
	trap	#1
	addq.l	#4,a7
	movem.l (a7)+,d1/a1
	rts

*--- lprint ---*
* parameters  a0  address of string to print
*	      d0  end of string (not printed)
* returns  nothing
* side effects	prints string on printer.

lprint	movem.l d1-d7/a1-a5,-(a7)
	movea.l a0,a1
	move.l	d0,d1
	sub.l	a1,d1			; compute length of string (d1)
	ble.s	2$			; don't print strings with negative len
1$	move.b	(a1)+,d0		; fetch char from string
	andi.w	#$7F,d0			; kill high bit
	bsr.s	cprnout			; print char
	subq.w	#1,d1			; len--
	bgt.s	1$			; next char, if any
2$	move.w	#13,d0			; print \n
	bsr.s	cprnout
	move.w	#10,d0			; print \r
	bsr.s	cprnout
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- donuttin ---*

donuttin
	rts

*--- unused code ---*

	rts
	rts

*--- openstory -- bind for open_sto() ---*
* parameters  none;
* returns  0 on success, GEMDOS error (negative) else
* side effects	puts handle of story.dat into story_handle(a6).

openstory
	movem.l d1-d7/a1-a5,-(a7)
	jsr	open_sto		; this is C, returns 0 if no success
	tst.l	d0
	blt.s	1$			; less than 0 means error (GEMDOS)
	move.w	d0,story_handle(a6)		; save story handle
	clr.w	d0			; return 0 means success
1$	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- closestory -- uses C closef() ---*
* This routine closes the story.dat file. It doesn't take any parameters,
* but returns the GEMDOS error in d0 and it sets the error flag	 if it is
* set to EQ, the close was successful. (That is, if d0 is 0)

closestory
	movem.l d1-d7/a1-a5,-(a7)
	move.w	story_handle(a6),-(a7)
	jsr	closef
	addq.w	#2,a7
	tst.l	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- readblock ---*
* parameters  d0  block number
*	      a0  buffer address
* returns  0 on okay, traps error.
* side effects	reads the desired block from story.dat into the buffer.

readblock
	movem.l d1,-(a7)
	moveq.l #1,d1
	bsr.s	readitem
	movem.l (a7)+,d1
	rts

*--- readitem -- C bind for read_ite() ---*
* parameters  d0  block number (offset from start of file)
*	      d1  number of 512 byte blocks to read
*	      a0  buffer address
* returns  zero if okay, traps errors.
* side effects	the desired blocks from the story.dat file into the buffer.

readitem
	movem.l d2-d7/a1-a5,-(a7)
	bsr	utimes512		; d0 *= 512
	exg.l	d1,d0
	bsr	utimes512		; d1 *= 512
	exg.l	d1,d0
	move.l	a0,-(a7)		; address of buffer
	move.l	d1,-(a7)		; number of bytes to read
	move.l	d0,-(a7)		; offset from start of file
	move.w	story_handle(a6),-(a7)
	jsr	read_ite		; C function, returns 0 on okay
	add	#14,a7			; bug??? a7??
	tst.l	d0
	bne.s	1$			; error handling
	movem.l (a7)+,d2-d7/a1-a5
	rts
1$	lea.l	sfrerr,a0		; "Story file read error"
	bra	internal_error

*--- unused code ---*
*
* 0B0EA4	  MOVE.W    D1,-(A7)
* 0B0EA6	  CLR.W	    D1
* 0B0EA8	  LEA.L	    $B1E72,A0
* 0B0EAE	  BSR	    $printstdstring
* 0B0EB2	  BSR	    $fetchchar
* 0B0EB6	  CMPI.B    #$D,D0
* 0B0EBA	  BEQ.S	    $B0ED8
* 0B0EBC	  CMPI.B    #$60,D0
* 0B0EC0	  BLT.S	    $B0EC6
* 0B0EC2	  SUBI.B    #$20,D0
* 0B0EC6	  CMPI.B    #$41,D0
* 0B0ECA	  BLT.S	    $B0EB2
* 0B0ECC	  CMPI.B    #$5A,D0
* 0B0ED0	  BGT.S	    $B0EB2
* 0B0ED2	  MOVE.W    D0,D1
* 0B0ED4	  BSR	    $outputchar
* 0B0ED8	  BSR	    $newline
* 0B0EDC	  MOVE.W    D1,D0
* 0B0EDE	  MOVE.W    (A7)+,D1
* 0B0EE0	  RTS

*--- open_query_ovwrt ---*

;;; open_query_ovwrt - try to open save file and if it exests, ask user
;;; whether to overwrite it
	
open_query_ovwrt
	movem.l d1-d7/a1-a5,-(a7)
	move.w	#22,-(a7)
	pea.l	open_fna
	move.w	#$4E,-(a7)
	trap	#1
	add	#8,a7
	tst.l	d0
	beq.s	1$
	clr.w	d1
	bra.s	3$
1$	lea.l	exfile,a0
	bsr	printstdstring
	bsr	fetchchar
	clr.w	d1
	cmpi.b	#$59,d0			;'Y'
	beq.s	2$
	cmpi.b	#$79,d0			;'y'
	beq.s	2$
	moveq.l #1,d1
	move.b	#$4E,d0			;'N'
2$	bsr	outputchar
	bsr	newline
3$	move.w	d1,d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

;;; ask_save_fname - ask for a file name
;;; returns:	in flags register - return status
	
ask_save_fname
	movem.l d1-d7/a1-a5,-(a7)
	clr.w	-(a7)
	move.w	#1,-(a7)
	jsr	asksaven
	addq.l	#4,a7
	tst.w	d0
	bne.s	1$
	bsr.s	open_query_ovwrt
1$	tst.w	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- create_save_file -- bind for create_f ---*
* parameters
* returns
* side effects

create_save_file
	movem.l d1-d7/a1-a5,-(a7)
	jsr	create_f
	tst.l	d0
	blt.s	1$
	move.w	d0,save_handle(a6)
	clr.w	d0
	bra.s	2$
1$	move.l	d0,-(a7)
	bsr.s	LBL91
	move.l	(a7)+,d0
2$	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- LBL74 ---*
* parameters
* returns
* side effects

LBL74
	movem.l d2-d7/a1-a5,-(a7)
	bsr	utimes512
	exg.l	d1,d0
	bsr	utimes512
	exg.l	d1,d0
	move.l	a0,-(a7)
	move.l	d1,-(a7)
	move.l	d0,-(a7)
	move.w	save_handle(a6),-(a7)
	jsr	write_it
	add	#14,a7
	move.l	d0,-(a7)
	beq.s	2$
	bgt.s	1$
	bsr.s	LBL91
	bra.s	2$
1$	lea.l	nospace,a0
	bsr	printcrstring
2$	move.l	(a7)+,d0
	movem.l (a7)+,d2-d7/a1-a5
	rts

*--- LBL91 ---*

LBL91
	cmpi.w	#-13,d0
	bne.s	1$
	lea.l	wrtprote,a0
	bsr	printcrstring
1$	rts

*--- LBL75 ---*

LBL75
	movem.l d1-d7/a1-a5,-(a7)
	move.w	save_handle(a6),-(a7)
	jsr	close_fi
	addq.w	#2,a7
	tst.l	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- LBL77 ---*

LBL77
	movem.l d1-d7/a1-a5,-(a7)
	jsr	delete_f
	tst.l	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- LBL79 ---*

LBL79
	movem.l d1-d7/a1-a5,-(a7)
	clr.w	-(a7)
	clr.w	-(a7)
	jsr	asksaven
	addq.l	#4,a7
	tst.w	d0
	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- LBL80 ---*

LBL80
	movem.l d1-d7/a1-a5,-(a7)
	jsr	open_fil
	tst.l	d0
	blt.s	1$
	move.w	d0,save_handle(a6)
	clr.w	d0
1$	movem.l (a7)+,d1-d7/a1-a5
	rts

*--- LBL81 ---*

LBL81
	movem.l d2-d7/a1-a5,-(a7)
	bsr	utimes512
	exg.l	d1,d0
	bsr	utimes512
	exg.l	d1,d0
	move.l	a0,-(a7)
	move.l	d1,-(a7)
	move.l	d0,-(a7)
	move.w	save_handle(a6),-(a7)
	jsr	read_ite
	add	#14,a7
	tst.l	d0
	movem.l (a7)+,d2-d7/a1-a5
	rts

*--- LBL78 ---*

LBL78
	clr.w	d0
	bra.s	LLL_7

*--- LBL76 ---*

LBL76
	moveq.l #1,d0
LLL_7	movem.l d1-d7/a1-a5,-(a7)
	move.w	d0,-(a7)
	jsr	file_pat	; C fun
	addq.l	#2,a7
	movem.l (a7)+,d1-d7/a1-a5
	rts

;;; storyopcl - open or close the story file
;;; parameters:	 d0 - 0 - close story file
;;;                   1 - open story file

storyopcl
	movem.l d1/a1,-(a7)	; book-keeping
	move.w	d0,d1		; redundant measure?
1$	tst.w	d1		; check parameter
	bne.s	2$		; = 0 - close story file
	bsr	closestory	; 
	beq.s	4$		; done if return value = 0
	bra.s	3$		; else prompt to insert story disk
2$	bsr	openstory	; open the story file
	beq.s	4$		; done
3$	lea.l	insdisk,a0	; else "insert the story disk ..."
	bsr	printstdstring	; print it
	bsr	fetchchar	; wait
	bsr	newline		; print a new line
	bra.s	1$		; try again
4$	movem.l (a7)+,d1/a1	; undo book-keeping
	rts

*--- my_timer ---*

my_timer
	move.w	d0,-(a7)
	move.w	milli_se,d0
	add.w	d0,milli_co
	move.w	(a7)+,d0
	move.l	old_time,-(a7)
	rts

*--- debug function interface ---*

dbugbc
	movem.l d0-d7/a0-a6,-(a7)
	move.w	d0,-(a7)
*	jsr	debugbc
	addq.l	#2,a7
	movem.l (a7)+,d0-d7/a0-a6
	rts

*--- The function tables ---*

	DATA
;;; 0OP opcodes
funtab1 dc.w	RTRUEop-bytecode_base
	dc.w	RFALSEop-bytecode_base
	dc.w	PRINTop-bytecode_base
	dc.w	PRINT_RETop-bytecode_base
	dc.w	NOPop-bytecode_base
	dc.w	SAVEop-bytecode_base
	dc.w	RESTOREop-bytecode_base
	dc.w	RESTARTop-bytecode_base
	dc.w	RET_POPPEDop-bytecode_base
	dc.w	POPop-bytecode_base
	dc.w	QUITop-bytecode_base
	dc.w	NEW_LINEop-bytecode_base
	dc.w	SHOW_STATUSop-bytecode_base
	dc.w	VERIFYop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base

;;; 1OP opcodes
funtab2 dc.w	JZop-bytecode_base
	dc.w	GET_SIBLINGop-bytecode_base
	dc.w	GET_CHILDop-bytecode_base
	dc.w	GET_PARENTop-bytecode_base
	dc.w	GET_PROP_LENop-bytecode_base
	dc.w	INCop-bytecode_base
	dc.w	DECop-bytecode_base
	dc.w	PRINT_ADDRop-bytecode_base
	dc.w	ILLEGALop2-bytecode_base
	dc.w	REMOVE_OBJop-bytecode_base
	dc.w	PRINT_OBJop-bytecode_base
	dc.w	RETop-bytecode_base
	dc.w	JUMPop-bytecode_base
	dc.w	PRINT_PADDRop-bytecode_base
	dc.w	LOADop-bytecode_base
	dc.w	NOTop-bytecode_base

;;; 2OP opcodes
funtab3 dc.w	ILLEGALop-bytecode_base
	dc.w	JEop-bytecode_base
	dc.w	JLop-bytecode_base
	dc.w	JGop-bytecode_base
	dc.w	DEC_CHKop-bytecode_base
	dc.w	INC_CHKop-bytecode_base
	dc.w	JINop-bytecode_base
	dc.w	TESTop-bytecode_base
	dc.w	ORop-bytecode_base
	dc.w	ANDop-bytecode_base
	dc.w	TEST_ATTRop-bytecode_base
	dc.w	SET_ATTRop-bytecode_base
	dc.w	CLEAR_ATTRop-bytecode_base
	dc.w	STOREop-bytecode_base
	dc.w	INSERT_OBJop-bytecode_base
	dc.w	LOADWop-bytecode_base
	dc.w	LOADBop-bytecode_base
	dc.w	GET_PROPop-bytecode_base
	dc.w	GET_PROP_ADDRop-bytecode_base
	dc.w	GET_NEXT_PROPop-bytecode_base
	dc.w	ADDop-bytecode_base
	dc.w	SUBop-bytecode_base
	dc.w	MULop-bytecode_base
	dc.w	DIVop-bytecode_base
	dc.w	MODop-bytecode_base
	dc.w	ILLEGALop1-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
;;; VAR opcodes
	dc.w	CALLop-bytecode_base
	dc.w	STOREWop-bytecode_base
	dc.w	STOREBop-bytecode_base
	dc.w	PUT_PROPop-bytecode_base
	dc.w	SREADop-bytecode_base
	dc.w	PRINT_CHARop-bytecode_base
	dc.w	PRINT_NUMop-bytecode_base
	dc.w	RANDOMop-bytecode_base
	dc.w	PUSHop-bytecode_base
	dc.w	PULLop-bytecode_base
	dc.w	SPLIT_WINDOWop-bytecode_base
	dc.w	SET_WINDOWop-bytecode_base
	dc.w	ILLEGALop3-bytecode_base
	dc.w	ILLEGALop4-bytecode_base
	dc.w	ILLEGALop5-bytecode_base
	dc.w	ILLEGALop6-bytecode_base
	dc.w	ILLEGALop7-bytecode_base
	dc.w	ILLEGALop8-bytecode_base
	dc.w	ILLEGALop9-bytecode_base
	dc.w	ILLEGALop10-bytecode_base ; OUTPUT_STREAM is V3!
	dc.w	ILLEGALop11-bytecode_base ; INPUT_STREAM is V3!
	dc.w	ILLEGALop12-bytecode_base ; SOUND_EFFECT is 3/5
	dc.w	ILLEGALop13-bytecode_base
	dc.w	ILLEGALop14-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base
	dc.w	ILLEGALop-bytecode_base

wrongz		dc.b "Wrong Z-machine version",0
strange		dc.b $20,$09,$0d,$2e,$2c,$3f,0
noputp		dc.b "Non-existant put property",0
nonext		dc.b "Non-existant next property",0
manyword	dc.b "Too many words for internal buffer",0
longcmd		dc.b "Command too long for internal buffer",0
scores		dc.b "Score: ",0
times		dc.b "Time:  ",0
badsave		dc.b "Wrong save file version",0
savfrerr	dc.b "Save file read error",0
banner		dc.b "Atari ST interpreter version A",0
badop		dc.b "Bad operation",0
alphab		dc.b "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		dc.b "	0123456789.,!?_#",$27,$22,"/\-:()",0
opners		dc.b "Story file open error",0
nomem		dc.b "Not enough memory",0
hitkys		dc.b "Strike any key to exit ",0
mores		dc.b "[MORE]",0
spaces		dc.b "	    ",0
sfrerr		dc.b "Story file read error",0
exfile		dc.b "You are about to write over an existing file.  "
		dc.b "Proceed? (Y/N) ",0
nospace		dc.b "Not enough room on disk",0
wrtprote	dc.b "Disk is write-protected",0
insdisk		dc.b "Insert the story disk and strike any key to continue ",0
inters		dc.b "Internal Error ",0

	END

*--- end of file ---*
