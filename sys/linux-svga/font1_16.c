/* binary image of 16-line GEM font 501 "Beyond Zork     Font 1" */

#include "gem_text.h"

static unsigned char bitmap[] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03, 0x03, 0x03, 
  0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x30, 0x30, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x00, 0x1E, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x19, 0x19, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x02, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x99, 0xC0, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x33, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x03, 0x03, 0x03, 0x03, 0x03, 
  0x03, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x30, 0x30, 0x30, 0x30, 0x30, 
  0x18, 0x18, 0x18, 0x18, 0x18, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x1E, 
  0x1E, 0x1E, 0x1E, 0x0E, 0x33, 0x00, 0x00, 0x99, 0x19, 0x19, 0x3D, 0x3D, 
  0x3D, 0x00, 0x0F, 0x80, 0x00, 0x00, 0x39, 0x00, 0x00, 0x07, 0x00, 0x00, 
  0x01, 0x99, 0x86, 0x60, 0x60, 0x8C, 0x00, 0x03, 0x0C, 0xC1, 0x98, 0x00, 
  0x00, 0x00, 0x00, 0x0C, 0x3F, 0x00, 0xC0, 0x3F, 0x0F, 0xFC, 0x01, 0x0F, 
  0xFC, 0x3F, 0x0F, 0xFC, 0x3F, 0x03, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x01, 
  0xE0, 0xF8, 0x03, 0x01, 0xF0, 0x07, 0x83, 0xE0, 0x7F, 0x9F, 0xE0, 0x78, 
  0x30, 0xCC, 0x00, 0xCC, 0x61, 0x80, 0x60, 0x66, 0x30, 0x3C, 0x0F, 0x80, 
  0x3C, 0x0F, 0x80, 0xF8, 0x7F, 0xE6, 0x06, 0x61, 0x98, 0x06, 0x63, 0x30, 
  0xCF, 0xE7, 0x98, 0x07, 0x82, 0x00, 0x01, 0x80, 0x00, 0xC0, 0x00, 0x00, 
  0x0C, 0x00, 0x07, 0x00, 0x06, 0x00, 0xC1, 0x98, 0x0C, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x06, 0x18, 0x60, 0x00, 0x3F, 0x1E, 0x1E, 0x1E, 0x1E, 
  0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 
  0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x0E, 0x00, 0x00, 0x33, 0x33, 0x33, 0x33, 
  0x33, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x18, 0x18, 0x18, 0x18, 0x18, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 
  0x1E, 0x1E, 0x1E, 0x1E, 0x33, 0x33, 0x33, 0x33, 0x33, 0x1B, 0x1E, 0x00, 
  0x1E, 0xBD, 0x3D, 0x3D, 0x26, 0x26, 0x26, 0x00, 0x1F, 0x80, 0x1E, 0x30, 
  0x1E, 0x30, 0x3E, 0x0F, 0x00, 0x00, 0x01, 0x99, 0x86, 0x60, 0x61, 0xCC, 
  0x1C, 0x03, 0x18, 0x61, 0x98, 0x18, 0x00, 0x00, 0x00, 0x18, 0x7F, 0x80, 
  0xC0, 0x7F, 0x8F, 0xFC, 0x03, 0x0F, 0xFC, 0x7F, 0x8F, 0xFC, 0x7F, 0x87, 
  0xF8, 0x00, 0x03, 0x80, 0x07, 0x03, 0xF1, 0xFC, 0x03, 0x01, 0xFC, 0x1F, 
  0xE3, 0xF8, 0x7F, 0x9F, 0xE1, 0xFE, 0x30, 0xCC, 0x00, 0xCC, 0x61, 0x80, 
  0x60, 0x66, 0x30, 0xFF, 0x0F, 0xE0, 0xFF, 0x0F, 0xE1, 0xFC, 0x7F, 0xE6, 
  0x06, 0x61, 0x98, 0x06, 0x63, 0x30, 0xCF, 0xE7, 0x8C, 0x07, 0x87, 0x00, 
  0x01, 0x80, 0x00, 0xC0, 0x00, 0x00, 0x0C, 0x00, 0x0F, 0x80, 0x06, 0x00, 
  0xC1, 0x98, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x18, 0x60, 
  0x00, 0x21, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 
  0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 
  0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x33, 0x33, 0x33, 0x0C, 0x00, 0x00, 0x1B, 
  0x00, 0x00, 0x33, 0x33, 0x33, 0x00, 0x00, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x33, 0x33, 0x33, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x0E, 0x0C, 0x00, 0x3F, 0x26, 0x26, 0x26, 0x00, 0x00, 
  0x00, 0x00, 0x3E, 0x00, 0x3F, 0x30, 0x0E, 0x30, 0x3F, 0x1C, 0x00, 0x00, 
  0x01, 0x99, 0x9F, 0xF8, 0xF8, 0x98, 0x3E, 0x06, 0x38, 0x70, 0xF0, 0x18, 
  0x00, 0x00, 0x00, 0x18, 0xE1, 0xC1, 0xC0, 0xE1, 0xC0, 0x38, 0x07, 0x0C, 
  0x00, 0xE1, 0xC0, 0x1C, 0xE1, 0xCE, 0x1C, 0x00, 0x07, 0x1F, 0xE3, 0x83, 
  0x33, 0x86, 0x07, 0x81, 0x8C, 0x38, 0x73, 0x1C, 0x60, 0x18, 0x03, 0x87, 
  0x30, 0xCC, 0x00, 0xCC, 0xC1, 0x80, 0x70, 0xE7, 0x31, 0xC3, 0x8C, 0x71, 
  0xC3, 0x8C, 0x71, 0x8C, 0x06, 0x06, 0x06, 0x61, 0x98, 0x06, 0x36, 0x19, 
  0x80, 0xC6, 0x0C, 0x01, 0x87, 0x00, 0x00, 0xC0, 0x00, 0xC0, 0x00, 0x00, 
  0x0C, 0x00, 0x0C, 0x80, 0x06, 0x00, 0x00, 0x18, 0x0C, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x06, 0x18, 0x60, 0x00, 0x21, 0x33, 0x33, 0x33, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x00, 
  0x00, 0x00, 0x1E, 0x1E, 0x33, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 
  0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x3F, 0x3F, 0x1E, 0x33, 
  0x33, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x3F, 0x3F, 0x1E, 0x33, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x1E, 0x3F, 0x3F, 0x1E, 0x33, 0x00, 0x1E, 0x00, 
  0x33, 0x00, 0x00, 0x00, 0x1E, 0x33, 0x1E, 0x00, 0x36, 0x00, 0x33, 0x30, 
  0x17, 0x3E, 0x33, 0x18, 0x00, 0x00, 0x01, 0x99, 0x9F, 0xF9, 0xF8, 0x18, 
  0x36, 0x0C, 0x30, 0x30, 0xF0, 0x18, 0x00, 0x00, 0x00, 0x30, 0xC0, 0xC7, 
  0xC0, 0xC0, 0xC0, 0x70, 0x0F, 0x0C, 0x00, 0xC0, 0x00, 0x38, 0xC0, 0xCC, 
  0x0C, 0x00, 0x0E, 0x1F, 0xE1, 0xC3, 0x33, 0x02, 0x07, 0x81, 0x8C, 0x30, 
  0x33, 0x0C, 0x60, 0x18, 0x03, 0x03, 0x30, 0xCC, 0x00, 0xCC, 0xC1, 0x80, 
  0x70, 0xE7, 0x31, 0x81, 0x8C, 0x31, 0x81, 0x8C, 0x31, 0x80, 0x06, 0x06, 
  0x06, 0x33, 0x0C, 0xCC, 0x36, 0x19, 0x80, 0xC6, 0x06, 0x01, 0x8D, 0x80, 
  0x00, 0x61, 0xEC, 0xDE, 0x07, 0xC1, 0xEC, 0x3E, 0x0C, 0x0F, 0xA6, 0xF0, 
  0xC1, 0x99, 0x8C, 0xBC, 0xF0, 0xBC, 0x0F, 0x86, 0xF0, 0x3D, 0x97, 0x83, 
  0xC7, 0xE6, 0x19, 0x99, 0x99, 0x98, 0x66, 0x0C, 0xFF, 0x06, 0x18, 0x60, 
  0x00, 0x21, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x1E, 0x1E, 0x33, 0x3F, 0x3F, 0x33, 0x33, 
  0x00, 0x00, 0x1E, 0x1C, 0x33, 0x3F, 0x3F, 0x1E, 0x1E, 0x1C, 0x1E, 0x33, 
  0x33, 0x3F, 0x3F, 0x3F, 0x3F, 0x33, 0x33, 0x1E, 0x1E, 0x1C, 0x1E, 0x33, 
  0x3F, 0x3F, 0x3F, 0x3F, 0x33, 0x1E, 0x1E, 0x1C, 0x1E, 0x33, 0x3F, 0x3F, 
  0x3F, 0x3F, 0x33, 0x1E, 0x3F, 0x1E, 0xB3, 0x1E, 0x1E, 0x1E, 0x3F, 0x33, 
  0x3F, 0x3B, 0x36, 0x1E, 0x33, 0x3E, 0x03, 0x3F, 0x33, 0x18, 0x00, 0x00, 
  0x01, 0x99, 0x86, 0x61, 0x80, 0x30, 0x36, 0x00, 0x30, 0x33, 0xFC, 0xFF, 
  0x00, 0xFF, 0x00, 0x30, 0xC0, 0xC7, 0xC0, 0x01, 0xC0, 0xE0, 0x1F, 0x0F, 
  0xF0, 0xDF, 0x00, 0x70, 0xE1, 0xCC, 0x0C, 0xC3, 0x1C, 0x00, 0x00, 0xE0, 
  0x63, 0x3A, 0x0C, 0xC1, 0xF8, 0x70, 0x03, 0x0E, 0x7E, 0x1F, 0x87, 0x00, 
  0x3F, 0xCC, 0x00, 0xCD, 0x81, 0x80, 0x79, 0xE7, 0xB3, 0x81, 0xCC, 0x33, 
  0x81, 0xCC, 0x31, 0xE0, 0x06, 0x06, 0x06, 0x33, 0x0C, 0xCC, 0x1C, 0x0F, 
  0x01, 0x86, 0x06, 0x01, 0x8D, 0x80, 0x00, 0x03, 0xFC, 0xFF, 0x0F, 0xE3, 
  0xFC, 0x7F, 0x1F, 0x1F, 0xE7, 0xF8, 0xC1, 0x9B, 0x8C, 0xFF, 0xF8, 0xFE, 
  0x1F, 0xC7, 0xF8, 0x7F, 0x9F, 0xC7, 0xE7, 0xE6, 0x19, 0x99, 0x99, 0x9C, 
  0xE6, 0x0C, 0xFF, 0x0E, 0x18, 0x70, 0x71, 0x21, 0x06, 0x06, 0x06, 0x06, 
  0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 
  0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x1F, 
  0x3F, 0x33, 0x33, 0x33, 0x33, 0x36, 0x0D, 0x2C, 0x3F, 0x1C, 0x33, 0x30, 
  0x0C, 0x1F, 0x3F, 0x1C, 0x3F, 0x33, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 
  0x33, 0x1F, 0x3F, 0x1C, 0x3F, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x1F, 
  0x3F, 0x1C, 0x3F, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x1F, 0x33, 0x3F, 
  0xB7, 0x1F, 0x3F, 0x3F, 0x33, 0x3B, 0x33, 0x3F, 0xB7, 0xBE, 0x30, 0x3F, 
  0x1F, 0x33, 0x33, 0x18, 0x00, 0x00, 0x01, 0x99, 0x86, 0x61, 0x80, 0x30, 
  0x1C, 0x00, 0x30, 0x33, 0xFC, 0xFF, 0x00, 0xFF, 0x00, 0x60, 0xC0, 0xC0, 
  0xC0, 0x03, 0x80, 0x70, 0x3B, 0x0F, 0xF8, 0xFF, 0x80, 0xE0, 0x7F, 0x8E, 
  0x1C, 0xC3, 0x38, 0x00, 0x00, 0x70, 0x63, 0x4A, 0x0C, 0xC1, 0xFC, 0x60, 
  0x03, 0x06, 0x7E, 0x1F, 0x86, 0x00, 0x3F, 0xCC, 0x00, 0xCD, 0x81, 0x80, 
  0x79, 0xE7, 0xB3, 0x00, 0xCC, 0x73, 0x00, 0xCC, 0x70, 0xF8, 0x06, 0x06, 
  0x06, 0x33, 0x0C, 0xCC, 0x1C, 0x0F, 0x01, 0x86, 0x03, 0x01, 0x98, 0xC0, 
  0x00, 0x07, 0x1C, 0xE3, 0x9C, 0x67, 0x1C, 0xE3, 0x9F, 0x38, 0xE7, 0x1C, 
  0xC1, 0x9F, 0x0C, 0xE7, 0x9C, 0xE7, 0x38, 0xE7, 0x1C, 0xE3, 0x9C, 0xC6, 
  0x61, 0x86, 0x19, 0x99, 0x99, 0x8F, 0xC6, 0x0C, 0x06, 0x3C, 0x18, 0x3C, 
  0xF9, 0x21, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 
  0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 
  0x06, 0x06, 0x06, 0x06, 0x06, 0x03, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 
  0x1B, 0x36, 0x33, 0x0C, 0x33, 0x30, 0x0C, 0x03, 0x33, 0x0C, 0x33, 0x33, 
  0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x03, 0x33, 0x0C, 0x33, 0x33, 
  0x33, 0x30, 0x0C, 0x33, 0x33, 0x03, 0x33, 0x0C, 0x33, 0x33, 0x33, 0x30, 
  0x0C, 0x33, 0x33, 0x03, 0x33, 0x33, 0x37, 0x03, 0x33, 0x33, 0x33, 0x3B, 
  0x33, 0x0C, 0xB7, 0xB0, 0x30, 0x33, 0x3F, 0x33, 0x33, 0x18, 0x00, 0x00, 
  0x01, 0x80, 0x1F, 0xF9, 0xF0, 0x60, 0x39, 0xC0, 0x30, 0x30, 0xF0, 0x18, 
  0x00, 0x00, 0x00, 0x60, 0xC0, 0xC0, 0xC0, 0x07, 0x00, 0x38, 0x73, 0x00, 
  0x1C, 0xE1, 0xC1, 0xC0, 0x7F, 0x87, 0xFC, 0x00, 0x1C, 0x1F, 0xE0, 0xE0, 
  0xC3, 0x4A, 0x18, 0x61, 0x8E, 0x60, 0x03, 0x06, 0x60, 0x18, 0x06, 0x0F, 
  0x30, 0xCC, 0x00, 0xCF, 0x01, 0x80, 0x6F, 0x66, 0xF3, 0x00, 0xCF, 0xE3, 
  0x00, 0xCF, 0xE0, 0x3C, 0x06, 0x06, 0x06, 0x1E, 0x07, 0xF8, 0x1C, 0x06, 
  0x03, 0x06, 0x03, 0x01, 0x98, 0xC0, 0x00, 0x06, 0x0C, 0xC1, 0x98, 0x06, 
  0x0C, 0xC1, 0x8C, 0x30, 0x66, 0x0C, 0xC1, 0x9E, 0x0C, 0xC3, 0x0C, 0xC3, 
  0x30, 0x66, 0x0C, 0xC1, 0x98, 0x07, 0x01, 0x86, 0x18, 0xF0, 0xFF, 0x07, 
  0x86, 0x0C, 0x0C, 0x3C, 0x18, 0x3C, 0x9F, 0x21, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1F, 
  0x33, 0x33, 0x3F, 0x33, 0x33, 0x33, 0x36, 0x1B, 0x33, 0x0C, 0x33, 0x3E, 
  0x0C, 0x1F, 0x33, 0x0C, 0x33, 0x33, 0x33, 0x3F, 0x3E, 0x0C, 0x33, 0x33, 
  0x1E, 0x1F, 0x33, 0x0C, 0x33, 0x33, 0x3F, 0x3E, 0x0C, 0x33, 0x33, 0x1F, 
  0x33, 0x0C, 0x33, 0x33, 0x3F, 0x3E, 0x0C, 0x33, 0x33, 0x1F, 0x3F, 0x37, 
  0x3B, 0x1F, 0x33, 0x33, 0x3F, 0x3F, 0x33, 0x0C, 0xBE, 0x30, 0x30, 0x33, 
  0x33, 0x3F, 0x33, 0x7E, 0x00, 0x00, 0x01, 0x80, 0x1F, 0xF8, 0xF8, 0x60, 
  0x6D, 0x80, 0x30, 0x30, 0xF0, 0x18, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 
  0xC0, 0x0E, 0x00, 0x1C, 0xE3, 0x00, 0x0C, 0xC0, 0xC1, 0x80, 0xE1, 0xC3, 
  0xEC, 0x00, 0x0E, 0x1F, 0xE1, 0xC0, 0xC3, 0x34, 0x1F, 0xE1, 0x86, 0x70, 
  0x03, 0x0E, 0x60, 0x18, 0x07, 0x0F, 0x30, 0xCC, 0x00, 0xCF, 0x81, 0x80, 
  0x6F, 0x66, 0xF3, 0x81, 0xCF, 0x83, 0x81, 0xCF, 0xC0, 0x0E, 0x06, 0x06, 
  0x06, 0x1E, 0x07, 0xF8, 0x1C, 0x06, 0x03, 0x06, 0x01, 0x81, 0x80, 0x00, 
  0x00, 0x06, 0x0C, 0xC1, 0x98, 0x06, 0x0C, 0xFF, 0x8C, 0x30, 0x66, 0x0C, 
  0xC1, 0x9F, 0x0C, 0xC3, 0x0C, 0xC3, 0x30, 0x66, 0x0C, 0xC1, 0x98, 0x03, 
  0xC1, 0x86, 0x18, 0xF0, 0xFF, 0x03, 0x06, 0x0C, 0x18, 0x0E, 0x18, 0x70, 
  0x8E, 0x21, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x3F, 0x33, 0x33, 0x3F, 0x33, 0x33, 0x33, 
  0x6C, 0x0D, 0xBF, 0x0C, 0x33, 0x3E, 0x0C, 0x3F, 0x3F, 0x0C, 0x33, 0x33, 
  0x33, 0x3F, 0x3E, 0x0C, 0x33, 0x33, 0x1E, 0x3F, 0x3F, 0x0C, 0x33, 0x33, 
  0x3F, 0x3E, 0x0C, 0x33, 0x33, 0x3F, 0x3F, 0x0C, 0x33, 0x33, 0x3F, 0x3E, 
  0x0C, 0x33, 0x33, 0x3F, 0x3F, 0x3F, 0x3B, 0x3F, 0x33, 0x33, 0x3F, 0x3F, 
  0x33, 0x3F, 0xBE, 0x30, 0x30, 0x33, 0x33, 0x3E, 0x7B, 0x18, 0x00, 0x00, 
  0x00, 0x00, 0x06, 0x60, 0x18, 0xC0, 0xC7, 0x00, 0x30, 0x31, 0x98, 0x18, 
  0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0x1C, 0x0C, 0x0C, 0xFF, 0xCC, 
  0x0C, 0xC0, 0xC3, 0x80, 0xC0, 0xC0, 0x0C, 0x00, 0x07, 0x00, 0x03, 0x80, 
  0xC3, 0x00, 0x30, 0x31, 0x86, 0x30, 0x33, 0x0C, 0x60, 0x18, 0x03, 0x03, 
  0x30, 0xCC, 0xC0, 0xCD, 0xC1, 0x80, 0x66, 0x66, 0x71, 0x81, 0x8C, 0x01, 
  0x8D, 0x8C, 0xE0, 0x06, 0x06, 0x06, 0x06, 0x1E, 0x07, 0xF8, 0x36, 0x06, 
  0x06, 0x06, 0x01, 0x81, 0x80, 0x00, 0x00, 0x06, 0x0C, 0xC1, 0x98, 0x06, 
  0x0C, 0xC0, 0x0C, 0x38, 0xE6, 0x0C, 0xC1, 0x9B, 0x0C, 0xC3, 0x0C, 0xC3, 
  0x30, 0x66, 0x0C, 0xC1, 0x98, 0x00, 0xE1, 0x86, 0x18, 0xF0, 0xFF, 0x07, 
  0x87, 0x1C, 0x30, 0x06, 0x18, 0x60, 0x00, 0x21, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x33, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x36, 0x1B, 0x30, 0x0C, 0x33, 0x30, 
  0x0C, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 
  0x0C, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 
  0x30, 0x0C, 0x33, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x33, 0x3B, 
  0x33, 0x33, 0x33, 0x33, 0x33, 0x37, 0x33, 0x4C, 0x36, 0x30, 0x33, 0x33, 
  0x33, 0x30, 0x33, 0x18, 0x00, 0x00, 0x00, 0x00, 0x06, 0x60, 0x18, 0xC8, 
  0xC6, 0x00, 0x38, 0x71, 0x98, 0x00, 0x0C, 0x00, 0x01, 0x80, 0xE1, 0xC0, 
  0xC0, 0x38, 0x0E, 0x1C, 0xFF, 0xCE, 0x1C, 0xE1, 0xC3, 0x00, 0xE1, 0xCE, 
  0x1C, 0x03, 0x03, 0x80, 0x07, 0x00, 0x03, 0x86, 0x30, 0x31, 0x8E, 0x38, 
  0x73, 0x1C, 0x60, 0x18, 0x03, 0x87, 0x30, 0xCC, 0xE1, 0xCC, 0xE1, 0x80, 
  0x66, 0x66, 0x71, 0xC3, 0x8C, 0x01, 0xC6, 0x0C, 0x63, 0x8E, 0x06, 0x07, 
  0x0E, 0x0C, 0x03, 0x30, 0x36, 0x06, 0x06, 0x06, 0x00, 0xC1, 0x80, 0x00, 
  0x00, 0x07, 0x1C, 0xE3, 0x9C, 0x67, 0x1C, 0xE1, 0x8C, 0x1F, 0xE6, 0x0C, 
  0xC1, 0x99, 0x8C, 0xC3, 0x0C, 0xC3, 0x38, 0xE7, 0x1C, 0xE3, 0x98, 0x0C, 
  0x61, 0x97, 0x38, 0x60, 0x66, 0x0F, 0xC3, 0xFC, 0x60, 0x06, 0x18, 0x60, 
  0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x36, 
  0x1B, 0x36, 0x30, 0x0C, 0x3F, 0x30, 0x0C, 0x33, 0x30, 0x0C, 0x33, 0x33, 
  0x3F, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x0C, 0x33, 0x30, 0x0C, 0x33, 0x33, 
  0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x30, 0x0C, 0x33, 0x33, 0x33, 0x30, 
  0x0C, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x37, 
  0x33, 0x4C, 0x36, 0x30, 0x33, 0x33, 0x33, 0x30, 0x33, 0x18, 0x00, 0x00, 
  0x01, 0x80, 0x00, 0x01, 0xF9, 0x9C, 0xEF, 0x80, 0x18, 0x60, 0x00, 0x00, 
  0x0C, 0x00, 0x31, 0x80, 0x7F, 0x87, 0xF8, 0x7F, 0xC7, 0xF8, 0x03, 0x07, 
  0xF8, 0x7F, 0x87, 0x00, 0x7F, 0x87, 0xF8, 0xC3, 0x00, 0x00, 0x00, 0x00, 
  0xC1, 0xFC, 0x60, 0x19, 0xFC, 0x1F, 0xE3, 0xF8, 0x7F, 0x98, 0x01, 0xFF, 
  0x30, 0xCC, 0x7F, 0x8C, 0x71, 0xFE, 0x60, 0x66, 0x30, 0xFF, 0x0C, 0x00, 
  0xFF, 0x0C, 0x71, 0xFC, 0x06, 0x03, 0xFC, 0x0C, 0x03, 0x30, 0x63, 0x06, 
  0x0F, 0xE7, 0x80, 0xC7, 0x80, 0x0F, 0xF8, 0x03, 0xFC, 0xFF, 0x0F, 0xE3, 
  0xFC, 0x7F, 0x0C, 0x0F, 0x66, 0x0C, 0xC1, 0x99, 0xCC, 0xC3, 0x0C, 0xC3, 
  0x1F, 0xC7, 0xF8, 0x7F, 0x98, 0x0F, 0xE1, 0xF3, 0xF8, 0x60, 0x66, 0x1C, 
  0xE1, 0xEC, 0xFF, 0x06, 0x18, 0x60, 0x00, 0x21, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x3F, 
  0x3F, 0x3F, 0x33, 0x3F, 0x3F, 0x30, 0x0D, 0x2C, 0x3F, 0x1E, 0x1F, 0x3F, 
  0x3F, 0x3F, 0x3F, 0x1E, 0x3F, 0x3F, 0x1F, 0x33, 0x3F, 0x3F, 0x3F, 0x3F, 
  0x0C, 0x3F, 0x3F, 0x1E, 0x3F, 0x3F, 0x33, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 
  0x3F, 0x1E, 0x3F, 0x3F, 0x33, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x33, 0x7F, 
  0x3F, 0x3F, 0x33, 0x3F, 0x33, 0x33, 0x3F, 0x7F, 0xB7, 0xBF, 0x3F, 0x3F, 
  0x3F, 0x30, 0x3F, 0x3F, 0x80, 0x00, 0x01, 0x80, 0x00, 0x01, 0xF1, 0x88, 
  0x79, 0x80, 0x0C, 0xC0, 0x00, 0x00, 0x18, 0x00, 0x33, 0x00, 0x3F, 0x07, 
  0xF8, 0xFF, 0xC3, 0xF0, 0x03, 0x03, 0xF0, 0x3F, 0x06, 0x00, 0x3F, 0x03, 
  0xF0, 0xC6, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF8, 0x60, 0x19, 0xF0, 0x07, 
  0x83, 0xE0, 0x7F, 0x98, 0x00, 0x78, 0x30, 0xCC, 0x1E, 0x0C, 0x39, 0xFE, 
  0x60, 0x66, 0x30, 0x3C, 0x0C, 0x00, 0x3D, 0x8C, 0x38, 0xF8, 0x06, 0x00, 
  0xF0, 0x0C, 0x03, 0x30, 0x63, 0x06, 0x0F, 0xE7, 0x80, 0x67, 0x80, 0x0F, 
  0xF8, 0x01, 0xEC, 0xDE, 0x07, 0xC1, 0xEC, 0x3E, 0x0C, 0x30, 0xE6, 0x0C, 
  0xC9, 0x98, 0xCC, 0xC3, 0x0C, 0xC3, 0x0F, 0x86, 0xF0, 0x3D, 0x98, 0x07, 
  0xC0, 0xE1, 0xE8, 0x60, 0x66, 0x18, 0x66, 0x1C, 0xFF, 0x06, 0x18, 0x60, 
  0x00, 0x3F, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
  0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1F, 0x1E, 0x1F, 0x33, 0x1E, 0x1E, 0x60, 
  0x00, 0x00, 0x1F, 0x1E, 0x03, 0x3F, 0x3F, 0x1F, 0x1F, 0x1E, 0x1E, 0x1F, 
  0x03, 0x33, 0x3F, 0x3F, 0x1E, 0x1E, 0x0C, 0x1F, 0x1F, 0x1E, 0x1E, 0x1E, 
  0x33, 0x3F, 0x3F, 0x1E, 0x1E, 0x1F, 0x1F, 0x1E, 0x1E, 0x1F, 0x33, 0x3F, 
  0x3F, 0x1E, 0x1E, 0x1F, 0x33, 0x5E, 0x5E, 0x1F, 0x33, 0x1E, 0x33, 0x33, 
  0x1E, 0x3F, 0xB7, 0x9F, 0x1E, 0x3E, 0x1E, 0x30, 0x3E, 0x7F, 0x80, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x30, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x3F, 0xC0, 0x00, 0x0F, 0x80, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x06, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x07, 0xF8, 0x00, 0x03, 0x99, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x06, 0x30, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x80, 0x00, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x01, 0x80, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xF0, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x1C, 0x1C, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
};

static int character_offsets[189] = {
  0, /* ascii 0x20 */
  6, /* ascii 0x21 */
  10, /* ascii 0x22 */
  18, /* ascii 0x23 */
  30, /* ascii 0x24 */
  38, /* ascii 0x25 */
  47, /* ascii 0x26 */
  59, /* ascii 0x27 */
  65, /* ascii 0x28 */
  71, /* ascii 0x29 */
  77, /* ascii 0x2A */
  87, /* ascii 0x2B */
  97, /* ascii 0x2C */
  103, /* ascii 0x2D */
  113, /* ascii 0x2E */
  117, /* ascii 0x2F */
  127, /* ascii 0x30 */
  139, /* ascii 0x31 */
  151, /* ascii 0x32 */
  163, /* ascii 0x33 */
  175, /* ascii 0x34 */
  187, /* ascii 0x35 */
  199, /* ascii 0x36 */
  211, /* ascii 0x37 */
  223, /* ascii 0x38 */
  235, /* ascii 0x39 */
  247, /* ascii 0x3A */
  251, /* ascii 0x3B */
  257, /* ascii 0x3C */
  266, /* ascii 0x3D */
  276, /* ascii 0x3E */
  285, /* ascii 0x3F */
  293, /* ascii 0x40 */
  304, /* ascii 0x41 */
  318, /* ascii 0x42 */
  328, /* ascii 0x43 */
  341, /* ascii 0x44 */
  352, /* ascii 0x45 */
  362, /* ascii 0x46 */
  372, /* ascii 0x47 */
  385, /* ascii 0x48 */
  395, /* ascii 0x49 */
  399, /* ascii 0x4A */
  411, /* ascii 0x4B */
  422, /* ascii 0x4C */
  432, /* ascii 0x4D */
  444, /* ascii 0x4E */
  453, /* ascii 0x4F */
  467, /* ascii 0x50 */
  477, /* ascii 0x51 */
  491, /* ascii 0x52 */
  501, /* ascii 0x53 */
  512, /* ascii 0x54 */
  524, /* ascii 0x55 */
  536, /* ascii 0x56 */
  546, /* ascii 0x57 */
  560, /* ascii 0x58 */
  569, /* ascii 0x59 */
  579, /* ascii 0x5A */
  588, /* ascii 0x5B */
  594, /* ascii 0x5C */
  604, /* ascii 0x5D */
  610, /* ascii 0x5E */
  619, /* ascii 0x5F */
  630, /* ascii 0x60 */
  636, /* ascii 0x61 */
  647, /* ascii 0x62 */
  658, /* ascii 0x63 */
  668, /* ascii 0x64 */
  679, /* ascii 0x65 */
  690, /* ascii 0x66 */
  697, /* ascii 0x67 */
  708, /* ascii 0x68 */
  719, /* ascii 0x69 */
  723, /* ascii 0x6A */
  730, /* ascii 0x6B */
  739, /* ascii 0x6C */
  743, /* ascii 0x6D */
  759, /* ascii 0x6E */
  769, /* ascii 0x6F */
  780, /* ascii 0x70 */
  791, /* ascii 0x71 */
  802, /* ascii 0x72 */
  811, /* ascii 0x73 */
  820, /* ascii 0x74 */
  828, /* ascii 0x75 */
  838, /* ascii 0x76 */
  846, /* ascii 0x77 */
  858, /* ascii 0x78 */
  868, /* ascii 0x79 */
  879, /* ascii 0x7A */
  889, /* ascii 0x7B */
  898, /* ascii 0x7C */
  902, /* ascii 0x7D */
  911, /* ascii 0x7E */
  921, /* ascii 0x7F */
  929, /* ascii 0x80 */
  937, /* ascii 0x81 */
  945, /* ascii 0x82 */
  953, /* ascii 0x83 */
  961, /* ascii 0x84 */
  969, /* ascii 0x85 */
  977, /* ascii 0x86 */
  985, /* ascii 0x87 */
  993, /* ascii 0x88 */
  1001, /* ascii 0x89 */
  1009, /* ascii 0x8A */
  1017, /* ascii 0x8B */
  1025, /* ascii 0x8C */
  1033, /* ascii 0x8D */
  1041, /* ascii 0x8E */
  1049, /* ascii 0x8F */
  1057, /* ascii 0x90 */
  1065, /* ascii 0x91 */
  1073, /* ascii 0x92 */
  1081, /* ascii 0x93 */
  1089, /* ascii 0x94 */
  1097, /* ascii 0x95 */
  1105, /* ascii 0x96 */
  1113, /* ascii 0x97 */
  1121, /* ascii 0x98 */
  1129, /* ascii 0x99 */
  1137, /* ascii 0x9A */
  1145, /* ascii 0x9B */
  1153, /* ascii 0x9C */
  1161, /* ascii 0x9D */
  1169, /* ascii 0x9E */
  1177, /* ascii 0x9F */
  1185, /* ascii 0xA0 */
  1193, /* ascii 0xA1 */
  1201, /* ascii 0xA2 */
  1209, /* ascii 0xA3 */
  1217, /* ascii 0xA4 */
  1225, /* ascii 0xA5 */
  1233, /* ascii 0xA6 */
  1241, /* ascii 0xA7 */
  1249, /* ascii 0xA8 */
  1257, /* ascii 0xA9 */
  1265, /* ascii 0xAA */
  1273, /* ascii 0xAB */
  1281, /* ascii 0xAC */
  1289, /* ascii 0xAD */
  1297, /* ascii 0xAE */
  1305, /* ascii 0xAF */
  1313, /* ascii 0xB0 */
  1321, /* ascii 0xB1 */
  1329, /* ascii 0xB2 */
  1337, /* ascii 0xB3 */
  1345, /* ascii 0xB4 */
  1353, /* ascii 0xB5 */
  1361, /* ascii 0xB6 */
  1369, /* ascii 0xB7 */
  1377, /* ascii 0xB8 */
  1385, /* ascii 0xB9 */
  1393, /* ascii 0xBA */
  1401, /* ascii 0xBB */
  1409, /* ascii 0xBC */
  1417, /* ascii 0xBD */
  1425, /* ascii 0xBE */
  1433, /* ascii 0xBF */
  1441, /* ascii 0xC0 */
  1449, /* ascii 0xC1 */
  1457, /* ascii 0xC2 */
  1465, /* ascii 0xC3 */
  1473, /* ascii 0xC4 */
  1481, /* ascii 0xC5 */
  1489, /* ascii 0xC6 */
  1497, /* ascii 0xC7 */
  1505, /* ascii 0xC8 */
  1513, /* ascii 0xC9 */
  1521, /* ascii 0xCA */
  1529, /* ascii 0xCB */
  1537, /* ascii 0xCC */
  1545, /* ascii 0xCD */
  1553, /* ascii 0xCE */
  1561, /* ascii 0xCF */
  1569, /* ascii 0xD0 */
  1577, /* ascii 0xD1 */
  1585, /* ascii 0xD2 */
  1593, /* ascii 0xD3 */
  1601, /* ascii 0xD4 */
  1609, /* ascii 0xD5 */
  1617, /* ascii 0xD6 */
  1625, /* ascii 0xD7 */
  1633, /* ascii 0xD8 */
  1641, /* ascii 0xD9 */
  1649, /* ascii 0xDA */
  1657, /* ascii 0xDB */
  1665  /* ascii 0xDC */
};

struct gem_font gem_font_501_16 = {
  501, /* id */
  6, /* pt_size */
  "Beyond Zork     Font 1",
  32, /* first_ascii */
  219, /* last_ascii */
  13, /* base_to_top */
  13, /* base_to_ascent */
  8, /* base_to_half */
  2, /* base_to_descent */
  2, /* base_to_bottom */
  15, /* max_width */
  16, /* max_box_width */
  0, /* left_italic_offset */
  5, /* right_italic_offset */
  1, /* thickening */
  1, /* underline_height */
  0x5555, /* light_mask */
  0x9249, /* skew_mask */
  0|0|0|0, /* flags */
  0, /* hot_address */
  character_offsets, /* cot_address */
  bitmap, /* font_address */
  210, /* byte_width */
  16, /* scan_lines */
  0, /* next_font */
};
