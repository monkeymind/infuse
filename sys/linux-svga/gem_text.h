/* gem_text.h - svga text output routines.
 * This file is part of INFUSE.
 * Copyright (C) 1997, 1998 by Florian M. Weps.  All rights reserved.
 */

#ifndef GEM_FONT_SEEN
#define GEM_FONT_SEEN 1

typedef struct gem_font {
  int id;
  int pt_size;
  char *name;
  int first_ascii;
  int last_ascii;
  int base_to_top;
  int base_to_ascent;
  int base_to_half;
  int base_to_descent;
  int base_to_bottom;
  int max_width;
  int max_box_width;
  int left_italic_offset;
  int right_italic_offset;
  int thickening;
  int underline_height;
  int light_mask;
  int skew_mask;
  int flags;
#define SYSTEM_FONT(x)     (x.flags&1)
#define HAS_HOT(x)         (x.flags&2)
#define MOTOROLA_FONT(x)   (x.flags&4)
#define MONOSPACED_FONT(x) (x.flags&8)
  int *hot_address;
  int *cot_address;
  unsigned char *font_address;
  int byte_width;
  int scan_lines;
  struct gem_font *next_font;
} GEM_FONT;

/* font ids */

#define GEM_FONT_1 501
#define GEM_FONT_3 503
#define GEM_FONT_4 504

/* font sizes */

#define GEM_FONT_SMALL 8
#define GEM_FONT_TALL  16

/* font styles - these can be |'ed, but ROMAN doesn't do anything then */

#define GEM_FONT_ROMAN   0
#define GEM_FONT_BOLD    1
#define GEM_FONT_ITALIC  2
#define GEM_FONT_REVERSE 4

/* text justifications - these are for gem_justified_text */

#define GEM_LEFT_JUSTIFIED   0 /* same output at gem_text(), but erases box */
#define GEM_RIGHT_JUSTIFIED  2 /* flush right in box, whichis reased */
#define GEM_CENTER_JUSTIFIED 3 /* erase box, center */
#define GEM_FULL_JUSTIFIED   4 /* insert extra spaces to fill text */

/* functions */

void gem_init (void);
void gem_set_font (int id, int size, int fg, int bg, int style);
int gem_get_font_h (void);
int gem_get_char_w (unsigned char c);
int gem_get_string_w (unsigned char *s);
int gem_text (int x, int y, unsigned char *s);
void gem_justified_text (int x, int y, int w, int just, unsigned char *s);

#endif

/* log - RCS
 * $Log: gem_text.h,v $
 * Revision 1.1.1.1  2004/07/21 14:43:21  fmw
 * Imported sources
 *
 * Revision 1.3  1998/01/12 16:59:29  fmw
 * bolface and italics added
 *
 * Revision 1.2  1997/12/22 10:28:59  fmw
 * justified text support
 *
 * Revision 1.1  1997/12/19 15:51:45  fmw
 * Initial revision
 *
 */
