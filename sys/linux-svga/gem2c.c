/* gem2c - utility to convert a GEM bitmap font to C definitions.
 * This file is part of INFUSE.
 * Copyright (C) 1997 by Florian M. Weps.  All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>

/* header */

static int font_id;
static int font_size;
static char font_name[37];
static int first_ascii;
static int last_ascii;
static int base_to_top;
static int base_to_ascent;
static int base_to_half;
static int base_to_descent;
static int base_to_bottom;
static int max_width;
static int max_box_width;
static int left_italic_offset;
static int right_italic_offset;
static int thickening;
static int underline_height;
static int light_mask;
static int skew_mask;
static int flags;
#define SYSTEM_FONT (flags&1)
#define HAS_HOT (flags&2)
#define MOTOROLA_FONT (flags&4)
#define MONOSPACED_FONT (flags&8)
static long hot_address;
static long cot_address;
static long font_address;
static int byte_width;
static int scan_lines;
static long next_font_addr;

/* flags */

static int gl_flag = 0;

/* character offset table, horizontal offset table */

static int *cot;
static int *hot;

/* font bitmap */

static unsigned char *font_data;

/* read_word - read a 16-bit unsigned integer in big-endian format */

unsigned int
read_word (FILE *fp)
{
  unsigned int ret;
  ret = getc (fp) & 0xFF;
  ret |= (getc (fp) & 0xFF) << 8;
  return ret;
}

/* read_long - read a 32-bit unsigned integer in big-endian format */

unsigned long
read_long (FILE *fp)
{
  unsigned long ret;
  ret = getc (fp) & 0xFF;
  ret |= (getc (fp) & 0xFF) << 8;
  ret |= (unsigned long)(getc (fp) & 0xFF) << 16;
  ret |= (unsigned long)(getc (fp) & 0xFF) << 24;
  return ret;
}

/* read_header - read the 88-byte header of a GEM font file. */

static void
read_header (FILE *fp)
{
  int i;
  font_id = read_word (fp);
  font_size = read_word (fp);
  for (i = 0; i < 32; i++)
    font_name[i] = getc (fp);
  font_name[32] = '\0';
  first_ascii = read_word (fp);
  last_ascii = read_word (fp);
  base_to_top = read_word (fp);
  base_to_ascent = read_word (fp);
  base_to_half = read_word (fp);
  base_to_descent = read_word (fp);
  base_to_bottom = read_word (fp);
  max_width = read_word (fp);
  max_box_width = read_word (fp);
  left_italic_offset = read_word (fp);
  right_italic_offset = read_word (fp);
  thickening = read_word (fp);
  underline_height = read_word (fp);
  light_mask = read_word (fp);
  skew_mask = read_word (fp);
  flags = read_word (fp);
  hot_address = read_long (fp);
  cot_address = read_long (fp);
  font_address = read_long (fp);
  byte_width = read_word (fp);
  scan_lines = read_word (fp);
  next_font_addr = read_long (fp);
}

/* explain header - debugging function which prints the header in
 * human-readable form */

void
explain_header (FILE *fp)
{
  fprintf (fp, "font id: %d\n", font_id);
  fprintf (fp, "font_size: %d\n", font_size);
  fprintf (fp, "font_name: %s\n", font_name);
  fprintf (fp, "first_ascii: %d\n", first_ascii);
  fprintf (fp, "last_ascii: %d\n", last_ascii);
  fprintf (fp, "base_to_top: %d\n", base_to_top);
  fprintf (fp, "base_to_ascent: %d\n", base_to_ascent);
  fprintf (fp, "base_to_half: %d\n", base_to_half);
  fprintf (fp, "base_to_descent: %d\n", base_to_descent);
  fprintf (fp, "base_to_bottom: %d\n", base_to_bottom);
  fprintf (fp, "max_width: %d\n", max_width);
  fprintf (fp, "max_box_width: %d\n", max_box_width);  
  fprintf (fp, "left_italic_offset: %d\n", left_italic_offset);
  fprintf (fp, "right_italic_offset: %d\n", right_italic_offset);
  fprintf (fp, "thickening: %d\n", thickening);
  fprintf (fp, "underline_height: %d\n", underline_height);
  fprintf (fp, "light_mask: %d\n", light_mask);
  fprintf (fp, "skew_mask: %d\n", skew_mask);
  fprintf (fp, "flags: %d\n", flags);
  fprintf (fp, "\tsystem font: %s\n", SYSTEM_FONT ? "yes" : "no");
  fprintf (fp, "\thorizontal offset table: %s\n", HAS_HOT ? "yes" : "no");
  fprintf (fp, "\tbyte-order: %s\n", MOTOROLA_FONT ? "motorola" : "intel");
  fprintf (fp, "\tmonospaced font: %s\n", MONOSPACED_FONT ? "yes" : "no");
  fprintf (fp, "hot_address: %ld\n", hot_address);
  fprintf (fp, "cot_address: %ld\n", cot_address);
  fprintf (fp, "font_address: %ld\n", font_address);
  fprintf (fp, "byte_width: %d\n", byte_width);
  fprintf (fp, "scan_lines: %d\n", scan_lines);
  fprintf (fp, "next_font_addr: %ld\n", next_font_addr);
}

/* read_table - read the character/horizontal offset table for n
 * characters from file fp, at offset offset, and return a pointer to
 * the newly allocated array. The array must have n+1 entries, since
 * the character widths are computed from it, and there must be a
 * surplus entry at the end to compute the width of the last
 * character. */

static int *
read_table (FILE *fp, long offset, int n)
{
  int i;
  int *t;
  int *ret = malloc ((n + 1) * sizeof (*t));
  fseek (fp, offset, SEEK_SET);
  if (ret != NULL)
    for (i = 0, t = ret; i < n + 1; i++, t++)
      *t = read_word (fp);
  return ret;
}

/* read_font - read the font data from file fp at offset */

static unsigned char *
read_font (FILE *fp, long offset, int bytes_per_line, int lines)
{
  unsigned char *ret = malloc (bytes_per_line * lines);
  fseek (fp, offset, SEEK_SET);
  if (ret != NULL)
    if (fread (ret, bytes_per_line, lines, fp) != lines)
      {
	free (ret);
	return NULL;
      }
  return ret;
}

/* emit_gl_font - write C definitions compatible with SVGALIB/GL
 * fonts. This works only for monospaced fonts. The output bitmap is
 * one character wide, and all 256 characters are arranged in a tall
 * stack, with character 0 at the top. */

static void
emit_gl_font (FILE *fp, unsigned char *data, int *offsets, int height,
	      int line_width, int first_code, int n)
{
  int width = offsets[1] - offsets[0];
  int i, j, k;
  fprintf (fp, "/* binary image of gemfont%dx%d */\n\n", width, height);
  fprintf (fp, "static unsigned char __gemfont%dx%d[] = {\n",
	   width, height);
  /* pad out low undefined characters with zeros */
  for (i = 0; i < first_code; i++)
    {
      fprintf (fp, "\t");
      for (j = 0; j < width/8*height; j++)
	fprintf (fp, "0,");
      fprintf (fp, " /* ascii %d */\n", i);
    }
  /* emit the defined characters */
  for (i = first_code; i < first_code+n; i++)
    {
      fprintf (fp, "\t");
      for (j = 0; j < height; j++)
	{
	  for (k = 0; k < width/8; k++)
	    fprintf (fp, "%d,", (int)(data[(offsets[i-first_code]/8+k)
					  +j*line_width]));
	}
      fprintf (fp, " /* ascii %d */\n", i);
    }
  /* pad out high undefined characters with zeros */
  for (i = first_code + n; i < 256; i++)
    {
      fprintf (fp, "\t");
      for (j = 0; j < width/8*height; j++)
	fprintf (fp, "0,");
      fprintf (fp, " /* ascii %d */\n", i);
    }
  fprintf (fp, "};\n\nunsigned char *gemfont%dx%d = __gemfont%dx%d;\n",
	   width, height, width, height);
}

/* emit_gem_font - emit C definitions compatible with Infuse svga font
 * functions.  The bitmap is one character high and very wide,
 * consisting of all characters side-by-side.  Only the range of
 * bitmaps in the original font file is converted; there are no empty
 * `pad' characters.  The character offset table is also emitted, and
 * a structure is exported which contains all relevant information
 * about the font, including skew and bold and underline and light
 * info.  Thus, a VDI graphical text emulation is possible. */

static void
emit_gem_font (FILE *fp, unsigned char *data, int *c_offsets,
	       int *h_offsets, int height,
	       int line_width, int first_code, int n,
	       int point_size, char *name, int id,
	       int base_to_top,
	       int base_to_ascent,
	       int base_to_half,
	       int base_to_descent,
	       int base_to_bottom,
	       int char_width,
	       int box_width,
	       int left_skew,
	       int right_skew,
	       int thick,
	       int underline,
	       int light,
	       int skew,
	       int flags)
{
  int i, j;
  fprintf (fp, "/* binary image of %d-line GEM font %d \"%s\" */\n",
	   height, id, name);
  fprintf (fp, "\n#include \"gem_text.h\"\n");
  fprintf (fp, "\nstatic unsigned char bitmap[] = {");
  /* emit font data */
  for (i = 0; i < height; i++)
    {
      for (j = 0; j < line_width; j++)
	{
	  if ((i*line_width+j) % 12 == 0)
	    fprintf (fp, "\n  ");
	  fprintf (fp, "0x%.2X, ", data[i*line_width+j]);
	}
    }
  fprintf (fp, "\n};\n");
  /* emit character offset table */
  fprintf (fp, "\nstatic int character_offsets[%d] = {\n", n+1);
  for (i = 0; i <= n; i++) /* n+1 entries */
    fprintf (fp,
	     "  %d%c /* ascii 0x%.2X */\n",
	     c_offsets[i],
	     i < n ? ',' : ' ',
	     first_code + i);
  fprintf (fp, "};\n");
  /* emit horizontal offsets if available */
  if (h_offsets)
    {
      fprintf (fp, "\nstatic int horizontal_offsets[%d] = {\n", n);
      for (i = 0; i < n; i++) /* n entries */
	fprintf (fp,
		 "  %d%c /* ascii 0x%.2X */\n",
		 h_offsets[i],
		 i < n-1 ? ',' : ' ',
		 first_code + i);
      fprintf (fp, "};\n");
    }
  /* emit the header structure */
  fprintf (fp, "\nstruct gem_font gem_font_%d_%d = {\n", id, height);
  fprintf (fp, "  %d, /* id */\n", id);
  fprintf (fp, "  %d, /* pt_size */\n", point_size);
  fprintf (fp, "  \"%s\",\n", name);
  fprintf (fp, "  %d, /* first_ascii */\n", first_code);
  fprintf (fp, "  %d, /* last_ascii */\n", first_ascii + n - 1);
  fprintf (fp, "  %d, /* base_to_top */\n", base_to_top);
  fprintf (fp, "  %d, /* base_to_ascent */\n", base_to_ascent);
  fprintf (fp, "  %d, /* base_to_half */\n", base_to_half);
  fprintf (fp, "  %d, /* base_to_descent */\n", base_to_descent);
  fprintf (fp, "  %d, /* base_to_bottom */\n", base_to_bottom);
  fprintf (fp, "  %d, /* max_width */\n", char_width);
  fprintf (fp, "  %d, /* max_box_width */\n", box_width);
  fprintf (fp, "  %d, /* left_italic_offset */\n", left_skew);
  fprintf (fp, "  %d, /* right_italic_offset */\n", right_skew);
  fprintf (fp, "  %d, /* thickening */\n", thick);
  fprintf (fp, "  %d, /* underline_height */\n", underline);
  fprintf (fp, "  0x%.4X, /* light_mask */\n", light);
  fprintf (fp, "  0x%.4X, /* skew_mask */\n", skew);
  fprintf (fp, "  %d|%d|%d|%d, /* flags */\n", flags&1, flags&2,
	   flags&4, flags&8);
  fprintf (fp, "  %s, /* hot_address */\n",
	   h_offsets ? "horizontal_offsets" : "0");
  fprintf (fp, "  character_offsets, /* cot_address */\n");
  fprintf (fp, "  bitmap, /* font_address */\n");
  fprintf (fp, "  %d, /* byte_width */\n", line_width);
  fprintf (fp, "  %d, /* scan_lines */\n", height);
  fprintf (fp, "  0, /* next_font */\n");
  fprintf (fp, "};\n");
}

/* main - driver routine. 1 argument: the GEM font file name. Output
 * goes to stdout. */

int
main (int argc, char **argv)
{
  FILE *font;
  if (argc != 2 && argc != 3)
    {
    usage:
      fprintf (stderr, "usage: gem2c [-g] <GEM font file name>\n"
	       "\t-g: emit gl-compatible font\n");
      exit (EXIT_FAILURE);
    }
  if (argc == 2)
    font = fopen (argv[1], "rb");
  else
    {
      if (argv[1][0] == '-' && argv[1][1] == 'g')
	gl_flag = 1;
      else
	goto usage;
      font = fopen (argv[2], "rb");
    }
  if (font == NULL)
    {
      fprintf (stderr, "can't open font file\n");
      exit (EXIT_FAILURE);
    }
  read_header (font);
#if 0
  explain_header (stderr);
#endif
  cot = read_table (font, cot_address, last_ascii - first_ascii + 1);
  if (cot == NULL)
    {
      fprintf (stderr, "couldn't read character offset table\n");
      exit (EXIT_FAILURE);
    }
  if (HAS_HOT)
    hot = read_table (font, cot_address, last_ascii - first_ascii + 1);
  font_data = read_font (font, font_address, byte_width, scan_lines);
  if (font_data == NULL)
    {
      fprintf (stderr, "couldn't read font data\n");
      exit (EXIT_FAILURE);
    }
  if (byte_width % 2) /* MUST be an even number of bytes---GEM standard */
    {
      fprintf (stderr, "this font does not have an even byte width\n");
      exit (EXIT_FAILURE);
    }
  if (gl_flag)
    {
      if (!MONOSPACED_FONT || (cot[1]-cot[0])%8)
	{
	  fprintf (stderr, "can't convert proportional fonts and "
		   "fonts with non-integer byte widths yet\n");
	  exit (EXIT_FAILURE);
	}
      emit_gl_font (stdout, font_data, cot, scan_lines, byte_width,
		    first_ascii, last_ascii-first_ascii+1);
    }
  else
    emit_gem_font (stdout, font_data, cot, hot, scan_lines, byte_width,
		   first_ascii, last_ascii-first_ascii+1,
		   font_size, font_name, font_id,
		   base_to_top, base_to_ascent, base_to_half, base_to_descent,
		   base_to_bottom, max_width, max_box_width,
		   left_italic_offset, right_italic_offset, thickening,
		   underline_height, light_mask, skew_mask, flags);
  return EXIT_SUCCESS;
}

/* log - RCS
 * $Log: gem2c.c,v $
 * Revision 1.1.1.1  2004/07/21 14:43:21  fmw
 * Imported sources
 *
 * Revision 1.2  1997/12/30 17:10:56  fmw
 * emits variable-pitch GEM fonts
 *
 * Revision 1.1  1997/12/19 15:51:35  fmw
 * Initial revision
 *
 */

