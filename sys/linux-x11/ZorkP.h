/* ZorkP.c - Private data structures and support routines for the Zork widget
   */

#ifndef ZORKP_SEEN
#define ZORKP_SEEN

#include "Zork.h"
#include <X11/CoreP.h>

#define ZtRZorkResource "ZorkResource"

typedef struct {
  int empty;
} ZorkClassPart;

typedef struct _ZorkClassRec {
  CoreClassPart core_class;
  ZorkClassPart zork_class;
} ZorkClassRec;

extern ZorkClassRec zorkClassRec;

typedef struct {
  /* resources */
  Dimension rows;
  Dimension columns;
  Pixel foreground;
  Pixel black;
  Pixel red;
  Pixel green;
  Pixel yellow;
  Pixel blue;
  Pixel magenta;
  Pixel cyan;
  Pixel white;
  XFontStruct *fnt1;
  XFontStruct *fnt1b;
  XFontStruct *fnt1i;
  XFontStruct *fnt1bi;
  XFontStruct *fnt3;
  XFontStruct *fnt4;
  XFontStruct *fnt4b;
  XFontStruct *fnt4i;
  XFontStruct *fnt4bi;
  XtCallbackList input_callback;
  /* private state */
  /* none */
} ZorkPart;

typedef struct _ZorkRec {
  CorePart core;
  ZorkPart zork;
} ZorkRec;

#endif /* ZORKP_SEEN */

/* Log
   $Log: ZorkP.h,v $
   Revision 1.1.1.1  2004/07/21 14:43:21  fmw
   Imported sources
 */
