/* Zork.c - Data structures and support routines for the Zork widget
   */

#ifndef ZORK_SEEN
#define ZORK_SEEN

/* resource names not in <X11/StringDefs.h> */

#define ZtNrows    "rows"
#define ZtNcolumns "columns"

#define ZtNblack   "black"
#define ZtNred     "red"
#define ZtNgreen   "green"
#define ZtNyellow  "yellow"
#define ZtNblue    "blue"
#define ZtNmagenta "magenta"
#define ZtNcyan    "cyan"
#define ZtNwhite   "white"

#define ZtNfnt1   "fnt1"
#define ZtNfnt1b  "fnt1b"
#define ZtNfnt1i  "fnt1i"
#define ZtNfnt1bi "fnt1bi"
#define ZtNfnt3   "fnt3"
#define ZtNfnt4   "fnt4"
#define ZtNfnt4b  "fnt4b"
#define ZtNfnt4i  "fnt4i"
#define ZtNfnt4bi "fnt4bi"

#define ZtCZorkResource "ZorkResource"
#define ZtCRows         "Rows"
#define ZtCColumns      "Columns"

typedef struct _ZorkClassRec *ZorkWidgetClass;
typedef struct _ZorkRec      *ZorkWidget;

extern WidgetClass zorkWidgetClass;

#endif /* ZORK_SEEN */

/* Log
   $Log: Zork.h,v $
   Revision 1.1.1.1  2004/07/21 14:43:21  fmw
   Imported sources
 */
